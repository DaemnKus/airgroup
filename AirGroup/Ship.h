#pragma once
#ifndef _SHIP_H_
#define _SHIP_H_
#include <iostream>
#include <string>

class Weapon		//�������� ����������
{
private:
	std::string	name,			//�������� ������ 
			ammunition;		//�������� ����������
	float destruction;		//����
	int type,				//��� ������
		firespeed,			//����������������
		quant;				//���-�� ��������
public:
	std::string getName() const { return name; }
	std::string getAmm() const { return ammunition; }
	int getfspeed() const { return firespeed; }
	float getdestr() const { return destruction; }
	int getType() const { return type; }
	int getQuant() const { return quant; }
	void setName(std::string &s) { name = s; }
	void setAmm(std::string &a) { ammunition = a; }
	void setfspeed(int &s) { firespeed = s; }
	void setdestr(float &d) { destruction = d; }
	void setQuant(int &q) { quant = q; }
	void setType(int &t){ type = t; }
	friend std::ostream & operator <<(std::ostream &, const Weapon &);
	Weapon() : name("") , ammunition(""), destruction(0),
				type(0), firespeed(0), quant(0){}
	Weapon(const std::string &s, const std::string &amm, float d = 0, int t = 0, int f = 0, int q = 0) :
		name(s), ammunition(amm), destruction(d), type(t), firespeed(f), quant(q){}
	Weapon(const Weapon &w): name(w.name), ammunition(w.ammunition), destruction(w.destruction),
							type(w.type), firespeed(w.firespeed), quant(w.quant){}
	//~Weapon();
};

struct Type{
	int type, curc, m[5];
};

class Plane				//�������� ��������
{
private:
	static const int WPcount = 5;		//������������ ���-�� �������� ����������
	int curwp = 0;
	Weapon weapon[WPcount];				//������� ����������
	Type type[3];			//���� ������� ����������
	float speed,						//�������� ��������
		consumption,					//������ �������
		fuel;							//����� �������
public:
	int getCw() const { return curwp; }
	const Weapon &getWeapon(int k = 0) const { return weapon[k]; }
	float getSpeed() const { return speed; }
	float getCons() const { return consumption; }
	float getFuel() const { return fuel; }
	Type getType(int k = 0) const { return type[k]; }
	void setCw(int cw) { curwp = cw; }
	void setSpeed(float s) { speed = s; }
	void setCons(float c) {consumption = c;}
	void setFuel(float f) { fuel = f; }
	void setWeapon(Weapon &, int = 0);
	float time() const{ if (!consumption || !speed) throw std::exception("Cannot count"); 
						float d = fuel / consumption; d = d / speed; return d; }
	float maxdistance() const {
		if (!consumption) throw std::exception("Cannot count");
								return (fuel / consumption); }
	Plane();
	Plane(float speed, float consumption, float fuel) : speed(speed), consumption(consumption), fuel(fuel){
		Weapon w;
		for (int i = 0; i < WPcount; i++){
			weapon[i] = w;
		}
		for (int i = 0; i < 3; i++){
			type[i] = { 0, 0, { 0, 0, 0, 0, 0 } };
		}
	}
	Plane(const Plane&);
	//~Plane();
	friend std::ostream & operator <<(std::ostream &, const Plane &);
};

struct Cap{
	std::string	rank,		//������
	name,		//���
	srnm,		//�������
	midnm;		//��������
		int	exp;		//����
	friend std::ostream & operator <<(std::ostream &os, const Cap &c);
};

class Ship				//�������� �������
{
private:
	std::string name;		//��� �������
	Cap captain;		//���������� � ��������
	float speed,		//�������� ������� 
		consumption,	//������ �������
		fuel;			//����� �������
	int member;			//���-�� ������ �������
	static const int WPcount = 25;		//���-�� ����� ����������
	int curwp = 0;
	Weapon weapon[WPcount];				//����������

public:
	Ship();
	Ship(const std::string &name, Cap &captain, float &speed, float &consumption, float &fuel, int &member) :
		name(name), captain(captain), speed(speed), consumption(consumption), fuel(fuel), member(member){
		Weapon w;
		for (int i = 0; i < WPcount; i++){
			weapon[i] = w;
		}
	}
	Ship(const Ship&);
	virtual ~Ship();
	std::string getName() const;
	void setName(std::string &);
	Cap getCap() const;
	void setCap(Cap &);
	float	getSpeed() const,
			getCons() const,
			getFuel() const;
	void	setSpeed(float &),
			setCons(float &),
			setFuel(float &),
			setMemb(int &);
	int getMemb() const,
		getCw() const;
	float maxdistance() const;
	//virtual Plane &getPlanes() const = 0;
	const Weapon &getWeapon(int k = 0) const;
	//virtual void setPlane(Plane, int = 0) = 0;
	void setWeapon(Weapon &, int = 0);
	//virtual Ship &getcoveringship() const = 0;
	//virtual void setcoveringship(Ship &) = 0;
	float firetime() const;
	void setCw(int cw) { curwp = cw; }
	//virtual float countpldestruction() const = 0;
	//virtual float countdestruction() const = 0;
	virtual Ship *clone() const = 0;
	virtual int iAm() const = 0;
	friend std::ostream & operator <<(std::ostream &, const Ship &);
protected:
	virtual std::ostream & print(std::ostream &) const = 0;
};

class Aircarrier :		//�������� ���������
	virtual public Ship
{
private:
	static const int PLcount = 10;		//���������� ���������			
	Plane planes[PLcount];				//�������� ���������	
public:
	Aircarrier();
	Aircarrier(const std::string &name, Cap &captain, float &speed, float &consumption, float &fuel, int &member) :
		Ship(name, captain, speed, consumption, fuel, member){
		Plane p;
		for (int i = 0; i < PLcount; i++){
			planes[i] = p;
		}
	}
	~Aircarrier();
	Aircarrier(const Aircarrier &ac) :Ship(ac), curpl(ac.curpl){
		for (int i = 0; i < PLcount; i++){
			planes[i] = ac.planes[i];
		}
	}
	const Plane &getPlane(int k = 0) const{ return planes[k]; }
	Plane &modPlane(int k){ return planes[k]; }
	int getpl() const{ return curpl; }
	void setpl(int p){ curpl = p; }
	void setPlane(Plane, int = 9);
	float countpldestruction() const;
	virtual Ship *clone() const { return new Aircarrier(*this); };
	friend std::ostream & operator <<(std::ostream &, const Aircarrier &);
	//virtual Ship &getcoveringship() const {}
	//virtual void setcoveringship(Ship &) { return; }
	//virtual float countdestruction() const { return 0; }
	virtual int iAm() const{ return 1; }
protected:
	int curpl = 0;
	virtual std::ostream & print(std::ostream &) const;
};

class Covership :		//�������� ����� ���������
	virtual public Ship
{
private:
	Ship *coveringship;					//������������ �������
public:
	Covership();
	Covership(const std::string &name, Cap &captain, float &speed, float &consumption, float &fuel, int &member, Ship * coveredship = nullptr) :
		Ship(name, captain, speed, consumption, fuel, member), coveringship(coveredship){}
	~Covership();
	Covership(const Covership &cs) : Ship(cs), coveringship(cs.coveringship){}
	const Ship *getcoveringship() const;
	void setcoveringship(Ship *);
	float countdestruction() const;
	virtual Ship *clone() const { return new Covership(*this); };
	friend std::ostream & operator <<(std::ostream &, const Covership &);
	virtual int iAm() const { return 2; }
protected:
	virtual std::ostream & print(std::ostream &) const;
};

class Aircover :
	public Aircarrier,
	public Covership
{
public:
	Aircover() :Aircarrier(), Covership(), Ship(){}
	Aircover(const std::string &name, Cap &captain, float &speed, float &consumption, float &fuel, int &member, Ship * coveredship = nullptr) :
		Aircarrier(), Covership(), Ship(name, captain, speed, consumption, fuel, member){
		setcoveringship(coveredship);
	}
	~Aircover();
	virtual Ship *clone() const { return new Aircover(*this); };
	friend std::ostream & operator <<(std::ostream &, const Aircover &);
	virtual int iAm() const { return 3; }
protected:
	virtual std::ostream & print(std::ostream &) const;
};


#endif