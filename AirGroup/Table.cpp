#include "stdafx.h"
#include "Table.h"

/*std::ostream & operator <<(std::ostream &os, const std::pair<std::string, Ship *> &p){
	os << '"' << p.first << '"';
	if (p.second)
		os << " - " << (*p.second);
	else
		os << "is empty";
	return os;
}*/

template <class sign, class data>
std::ostream &operator <<(std::ostream &os, const Pair<sign,data> &p){
	os << '"' << p.first << '"';
	if (p.second)
		os << " - " << (*p.second);
	else
		os << " is empty";
	return os;
}

/*template <class sign, class data>
Pair<sign,data>::Pair(const Pair<sign,data> &p) :first(p.first)
{
	second = p.second ? p.second->clone() : nullptr;
}

template <class sign, class data>
Pair<sign, data>::Pair(Pair<sign, data> &&p) : first(p.first)
{
	second = p.second;
	p.second = nullptr;
}*/

template <class sign, class data>
Pair<sign, data> & Pair<sign, data>::operator =(const Pair<sign, data> &p)
{
	if (this != &p){
		delete second;
		first = p.first;
		second = p.second ? p.second->clone() : nullptr;
	}
	return *this;
}

template <class sign, class data>
Pair<sign, data> & Pair<sign, data>::operator =(Pair<sign, data> &&p)
{
	if (this != &p){
		delete second;
		first = p.first;
		second = p.second;
		p.second = nullptr;
	}
	return *this;
}

/*
template <class sign, class data>
Table<sign, data>::Table(const Table<sign, data> &a) :
cnt(a.cnt), maxsz(a.maxsz)
{
	arr = new Pair<sign,data>[a.maxsz];
	for (int i = 0; i < cnt; i++)
		arr[i] = a.arr[i];
}*/
/*
Table::~Table(){
	std::map<const std::string, Ship *>::iterator p;
	for (p = arr.begin(); p != arr.end(); ++p){ 
		delete p->second; 
		p->second = nullptr;
	}
}*/

/*template <class sign, class data>
Table<sign, data>::Table(Table<sign, data> &&a) :
cnt(a.cnt), maxsz(a.maxsz), arr(a.arr){
	a.arr = nullptr;
}*/

/*Table& Table::operator = (const Table &a){
	std::map<const std::string, Ship *>::iterator p;
	if (this != &a){
		for (p = arr.begin(); p != arr.end(); ++p)
			delete p->second;
		arr.clear();
		std::map<const std::string, Ship *>::const_iterator pp;
		for (pp = a.arr.begin(); pp != a.arr.end(); ++pp)
			arr.insert(make_pair(p->first, p->second->clone()));
	}
	return *this;
}*/

template <class sign, class data>
Table<sign,data> & Table<sign, data>::operator = (const Table<sign, data> &a){
	if (this != &a){
		delete[] arr;
		maxsz = a.maxsz;
		if (maxsz)
			arr = new Pair<sign, data>[maxsz];
		else
			arr = nullptr;
		cnt = a.cnt;
		for (int i = 0; i < cnt; i++)
			arr[i] = a.arr[i];
	}
	return *this;
}

template <class sign, class data>
Table<sign, data> & Table<sign, data>::operator = (Table<sign, data> &&a){
	if (this != &a){
		delete[] arr;
		arr = a.arr;
		a.arr = nullptr;
		maxsz = a.maxsz;
		cnt = a.cnt;
	}
	return *this;
}

template <class sign, class data>
void Table<sign, data>::insert(const sign &s, const data *f){
	/*std::map<const std::string, Ship *>::iterator p = arr.find(s);
	if (p == arr.end()){
		std::pair<std::map<const std::string, Ship *>::iterator, bool> pp = arr.insert(make_pair(s, f->clone()));
		if (!pp.second)
			throw std::exception("can't insert new item into map");
	}
	else{
		delete p->second;
		p->second = f->clone();
	}
	*/
	for (int i = 0; i < cnt; i++){
		if (arr[i].first == s){
			delete arr[i].second;
			arr[i].second = f->clone();
			return;
		}
	}
	Pair<sign, data> pp(s, *f);
	if (!pp.second)
		throw std::exception("Can't insert new item into Table");
	Pair<sign, data> *arr1 = new Pair<sign, data>[++cnt];
	for (int i = 0; i < cnt - 1; i++){
		arr1[i] = arr[i];
	}
	arr1[cnt - 1] = pp;
	delete[]arr;
	arr = arr1;
}

template <class sign, class data>
void Table<sign, data>::del(const sign &s){
	for (int i = 0; i < cnt; i++){
		if (arr[i].first == s){
			//delete arr[i].second;
			cnt--;
			if (cnt){
				Pair<sign, data> *arr1 = new Pair<sign, data>[cnt];
				
				for (int j = 0; j < i; j++){
					arr1[j] = arr[j];
					//delete arr[j].second;
				}
				for (int j = i; j < cnt;++j){
					arr1[j] = arr[j+1];
					//delete arr[j].second;
				}
				delete [] arr;
				arr = arr1;
			}
			else{
				delete [] arr;
				arr = nullptr;
			}
			return;
		}
	}
	throw std::exception("This ship is absent");
}

template <class sign, class data>
Pair <sign, data> & Table<sign, data>::operator [](const sign &s){
	int i;
	for (i = 0; i < cnt; i++){
		if (arr[i].first == s)
			return arr[i];
	}
	/*if (cnt == maxsz){
		Pair *cur = new Pair[maxsz += QUOTA];
		for (i = 0; i < cnt; i++){
			cur[i].first = arr[i].first;
			cur[i].second = arr[i].second;
			arr[i].second = nullptr;
		}
		delete[] arr;
		arr = cur;
	}
	arr[cnt].first = s;
	return arr[cnt++];*/
	//return Pair<sign, data>();
}

/*
const std::pair<std::string, Ship *> Table::operator [ ](const std::string &s) const{
	std::pair<std::string, Ship *> res;
	std::map<const std::string, Ship *>::const_iterator p = arr.find(s);
	return (p == arr.end()) ? res : (*p);
}*/

template <class sign, class data>
const Pair<sign, data> Table<sign, data>::operator [](const sign &s) const
{
	for (int i = 0; i < cnt; i++)
	if (arr[i].first == s)
		return arr[i];
	return Pair<sign, data>();
}

/*Ship* Table::getShip(const std::string &s) const{
	std::map<const std::string, Ship *>::const_iterator p;
	p = arr.find(s);
	if (p == arr.end())
		return nullptr;
	return (*p).second;
}*/

template <class sign, class data>
data * Table<sign, data>::getShip(const sign &s) const{
	for (int i = 0; i < cnt; i++){
		if (arr[i].first == s)
			return arr[i].second;
	}
	return nullptr;
}

template <class sign, class data>
int Table<sign, data>::getCnt() const{
	int ccnt = cnt;
	for (int i = 0; i < cnt; i++){
		int k = arr[i].second->iAm();
		if (k != 2){
			Aircarrier *ac = dynamic_cast<Aircarrier *>(arr[i].second);
			ccnt += ac->getpl();
			//continue;
		}
	}
	/*for (p = arr.begin(); p != arr.end(); p++){
		int k = (*p).second->iAm();
		if (k != 2){
			Aircarrier *ac = dynamic_cast<Aircarrier *>((*p).second);
			cnt += ac->getpl();
			//continue;
		}
		if (k == 3){
			Aircover *ac = dynamic_cast<Aircover *>((*p).second);
			cnt += ac->getpl();
			continue;
		}
	}*/
	return ccnt;
}

template <class sign, class data>
std::ostream & operator <<(std::ostream &os, const Table<sign, data> &t){
	/*std::map<const std::string, Ship *>::const_iterator p;

	for (p = t.arr.begin(); p != t.arr.end(); p++){
	os << (*p);
	}*/
	/*for (int i = 0; i < t.cnt; i++){
		os << t.arr[i];
	}
	return os;*/
	return t.print(os);
}

template <class sign,class data>
std::ostream & Table<sign, data>::print(std::ostream &os) const {
	for (int i = 0; i < cnt; i++){
		os << arr[i];
	}
	return os;
}
/*
template <class sign, class data>
Table<sign, data>::Iterator Table<sign, data>::find(sign &s) {
	Pair<sign,data> *p=nullptr;
	for (int i = 0; i < cnt; i++){
		if (arr[i].first == s)
		{
			p = &(arr[i]);
			break;
		}
	}
	return TableIt(p);
}

template <class sign, class data>
Table<sign, data>::Iterator Table<sign, data>::begin(){
	return TableIt(arr);
}

template <class sign, class data>
Table<sign, data>::Iterator Table<sign, data>::end(){
	return TableIt(arr+cnt);
}*/

template <class sign, class data>
int TableIt<sign, data>::operator !=(const TableIt<sign, data> &it) const{
	return cur != it.cur;
}
/*
template <class sign, class data>
std::ostream & operator <<(std::ostream &os, const Table<sign,data> &t){
	//std::map<const std::string, Ship *>::const_iterator p;
	Table<sign, data>::Iterator p;
	for (p = t.arr.begin(); p != t.arr.end(); p++){
		os << (*p);
	}
	return os;
}*/

template<class sign, class data>
int TableIt<sign, data>::operator ==(const TableIt<sign, data> &it) const{
	return cur == it.cur;
}

template <class sign, class data>
Pair <sign, data> & TableIt<sign, data>::operator *(){
	return *cur;
}

/*std::pair<const std::string, Ship *> & TableIt::operator *(){
	return *cur;
}*/
/*
template <class sign, class data>
Table <sign, data>::Iterator & Table <sign, data>::Iterator::operator ++(){
	++cur;
	return *this;
}

template <class sign, class data>
TableIt<sign, data> TableIt<sign, data>::operator ++(int) {
	TableIt res(*this);
	++cur;
	return res;
}*/



/*template <class e_sign, class s_sign>
Fleet<e_sign, s_sign>::Fleet()
{
	Table<e_sign, Table<s_sign, Ship>> t;
	esq = t;
	commander = { "Captain", "Ivan", "Ivanov", "Ivanovich", 7 };
}*/

template <class e_sign, class s_sign>
Ship & Fleet<e_sign, s_sign>::operator [](const s_sign &s){
	//Table<e_sign, Table<s_sign, Ship>>::Iterator it = esq.begin();
	TableIt<e_sign, Table<s_sign, Ship>> it = esq.begin();
	std::string err = "\""+s+"\" - Ship with this sign is absent";
	for (; it != esq.end(); it++){
		Ship *sh = (*it).second->getShip(s);
		if (sh)
			return *(sh);
	}
	throw std::exception(err.c_str());
}

template <class e_sign, class s_sign>
const Ship & Fleet<e_sign, s_sign>::operator [](const s_sign &s) const{
	Table<e_sign, Table<s_sign, Ship>>::const_Iterator it=esq.begin();
	//const_TableIt<e_sign, Table<s_sign, Ship>> it = &(esq[0]);//esq.begin();
	std::string err = "\"" + s + "\" - Ship with this sign is absent";
	for (; it != esq.end(); it++){
		Ship *sh = (*it).second->getShip(s);
		if (sh)
			return *(sh);
	}
	throw std::exception(err.c_str());
}

template <class e_sign, class s_sign>
const Plane &Fleet<e_sign, s_sign>::getPlane(const s_sign &s, int &k) const {
	const Ship *sh = &((*this)[s]);
	if (sh){
		if (sh->iAm() != 2){
			const Aircarrier *ac = dynamic_cast<const Aircarrier *>(sh);
			if (k < ac->getpl()){
				return ac->getPlane(k);
			}
			else
				throw std::exception("This plane is absent");
		}		
		throw std::exception("This ship has no any plane");
	}
	else throw std::exception("The ship with this sign doesn't exist");
}

template <class e_sign, class s_sign>
Plane & Fleet<e_sign, s_sign>::modPlane(const s_sign &s, int &k){
	Ship *sh = &((*this)[s]);
	if (sh){
		if (sh->iAm() != 2){
			Aircarrier *ac = dynamic_cast<Aircarrier *>(sh);
			if (k < ac->getpl()){
				return ac->modPlane(k);
			}
			else
				throw std::exception("This plane is absent");
		}
		/*if (sh->iAm() == 3){
			Aircover *ac = dynamic_cast<Aircover *>(sh);
			if (k < ac->getpl()){
				return ac->modPlane(k);
			}
			else
				throw std::exception("This plane is absent");
		}*/
		throw std::exception("This ship has no any plane");
	}
	else throw std::exception("The ship with this sign doesn't exist");
}

template <class e_sign, class s_sign>
inline int Fleet<e_sign, s_sign>::getcount(int t){
	int cnt = 0;
	//Table<e_sign, Table<s_sign, Ship>>::Iterator it = esq.begin();
	TableIt<e_sign, Table<s_sign, Ship>> it = esq.begin();
	for (; it != esq.end(); it++){
		Pair<e_sign, Table<s_sign, Ship>> *p = &(*it);
		Table<s_sign, Ship> *t1 = p->second;
		for (TableIt<s_sign, Ship> it2 = t1->begin(); it2 != t1->end(); it2++){
			Pair<s_sign, Ship> *p2 = &(*it2);
			Ship *t2 = p2->second;
			if (t2->iAm() == t)
				cnt++;
		}
	}
	return cnt;
}

template <class e_sign, class s_sign>
inline int Fleet<e_sign, s_sign>::getesqcount() const{ return esq.size(); }

template <class e_sign, class s_sign>
void Fleet<e_sign, s_sign>::insert(const s_sign &s, Ship &sh, const e_sign &k){
	int sz = esq.size();
	if (k < sz){		
		esq[k].second->insert(s, &sh);
	}
	else{
		const Table<s_sign,Ship> t;
		//std::vector<Table>::iterator p = esq.end();
		esq.insert(sz, &t);
		esq[esq.size() - 1].second->insert(s, &sh);
	} //throw std::exception("Esquadr with this number doesn't exist");
}

template <class e_sign, class s_sign>
void Fleet<e_sign, s_sign>::insert(const Plane &pl, const s_sign &s){
	Ship *sh = &((*this)[s]);
	if(sh){
		if (sh->iAm() != 2){
			Aircarrier *ac = dynamic_cast<Aircarrier *>(sh);
			if (ac->getpl() < 10){
				ac->setPlane(pl);
				return;
			}
			else
				throw std::exception("Cannot add more than 10 planes");
		}
		/*if (sh->iAm() == 3){
			Aircover *ac = dynamic_cast<Aircover *>(sh);
			if (ac->getpl() < 10){
				ac->setPlane(pl);
				return;
			}
			else
				throw std::exception("Cannot add more than 10 planes");
		}*/
		throw std::exception("This ship cannot have any plane");
	}
	else throw std::exception("The ship with this sign doesn't exist");
}

template <class e_sign, class s_sign>
void Fleet<e_sign, s_sign>::delShip(const s_sign &s){
	Table<e_sign, Table<s_sign, Ship>>::Iterator it = esq.begin();
	Table<s_sign, Ship>::Iterator it1;
	e_sign k;
	for (; it != esq.end(); it++){
		Ship *sh1 = (*it).second->getShip(s);
		if (sh1){
			k = (*it).first;
			(*it).second->del(s);
			break;
		}
	}
	int i = 0;
	
	if (!((*it).second->size()))
	{
		esq.del(k);
	}
	/*esq[k].del(s);
	it = esq[k].begin();
	int i = 0;
	for (; it != (esq[k]).end(); it++)
		i++;	
	if (!i){
		std::vector<Table>::iterator p = esq.begin();
		while (&(*p) != &esq[k] || p != esq.end())
			++p;
		esq.erase(p);
	}*/
}

template <class e_sign, class s_sign>
void Fleet<e_sign, s_sign>::delPlane(const s_sign &s, int &k){
	Ship *sh = &((*this)[s]);
	if (!sh){
		throw std::exception("Ship doesn't exist");
	}
	if (sh->iAm() == 2)
		throw std::exception("ship cannot have any plane");
	Aircarrier *ac = dynamic_cast<Aircarrier *>(sh);
	int p = ac->getpl()-1;
	for (int i = k; i < p; i++){
		ac->setPlane(ac->getPlane(k + 1), k);
	}
	Plane pl;
	ac->setPlane(pl, p);
	ac->setpl(p);
}

template <class e_sign, class s_sign>
void Fleet<e_sign, s_sign>::transport(const s_sign &dst, const s_sign &src, int k){
	Ship *sh1 = &((*this)[src]);
	Ship *sh2 = &((*this)[dst]);
	if (sh1->iAm() == 2)
		throw std::exception("Source ship cannot have any plane!");
	if (sh2->iAm() == 2)
		throw std::exception("Destination ship cannot have any plane!");
	insert(getPlane(src, k), dst);
	delPlane(src, k);
}

template <class e_sign, class s_sign>
void Fleet<e_sign, s_sign>::fueltrans(const s_sign &dst, const s_sign &src, float f){
	Ship *sh1 = &((*this)[src]);
	Ship *sh2 = &((*this)[dst]);
	float f1 = sh1->getFuel(), f2 = sh2->getFuel();
	if (f1 >= f){
		f1 -= f;
		f2 += f;
		sh2->setFuel(f2);
		sh1->setFuel(f1);
	}
	else throw std::exception("Have no enough fuel");
}

template <class e_sign, class s_sign>
float Fleet<e_sign, s_sign>::maxtime(){
	//int sz = esq.size();
	Table<e_sign, Table<s_sign, Ship>>::Iterator p = esq.begin();
	Table<s_sign, Ship>::Iterator it;
	float d = 0;
	for (; p != esq.end(); p++){
		for (it = (*p).second->begin(); it != (*p).second->end(); it++){
			Ship *sh = (*it).second;
			if (sh->iAm() != 2){
				Aircarrier *ac = dynamic_cast<Aircarrier *>(sh);
				for (int j = 0; j < ac->getpl(); j++){
					float t = ac->getPlane(j).time();
					if (t>d)
						d = t;
				}
			}
		}
	}
	return d;
}

template <class e_sign, class s_sign>
float Fleet<e_sign, s_sign>::radius(e_sign k){
	float rad = 0;
	int sz = esq.size();
	if (sz < k)
		return 0;
	Table<s_sign,Ship>::Iterator it;
	for (it = esq[k].second->begin(); it != esq[k].second->end(); it++){
		if ((*it).second->iAm() == 1 || (*it).second->iAm() == 3){
			Aircarrier *ac = dynamic_cast<Aircarrier *>((*it).second);
			float r1 = ac->maxdistance(), r2 = 0;
			for (int i = 0; i < ac->getpl(); i++){
				float r3 = ac->getPlane(i).maxdistance();
				if (r3>r2)
					r2 = r3;
			}
			r1 += r2;
			if (r1 > rad)
				rad = r1;
		}
	}
	return rad;
}

template <class e_sign, class s_sign>
const Table <s_sign, Ship> & Fleet<e_sign, s_sign>::getesq(const e_sign &i){ 
	//Pair <e_sign, Table<s_sign, Ship>> pp = esq[i];
	return *(esq[i].second); 
}

template <class e_sign, class s_sign>
const Table <s_sign, Ship> & Fleet<e_sign, s_sign>::nesq(const int &n) const{
	Table<e_sign, Table<s_sign, Ship>>::const_Iterator it = esq.begin();
	for (int i = 0; it != esq.end() && i < n; it++, i++){}
	if (it != esq.end())
		return *((*it).second);
	throw std::exception("This esquadr doesn't exist");
}
