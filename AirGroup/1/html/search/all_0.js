var searchData=
[
  ['aircarrier',['Aircarrier',['../class_aircarrier.html',1,'Aircarrier'],['../class_aircarrier.html#ad8bca63a8caf7fcacb2d1f8b2d7b57fc',1,'Aircarrier::Aircarrier()'],['../class_aircarrier.html#a95fa981343e4802908b31bed2b0b4564',1,'Aircarrier::Aircarrier(const std::string &amp;name, Cap &amp;captain, float &amp;speed, float &amp;consumption, float &amp;fuel, int &amp;member)'],['../class_aircarrier.html#abdfab7a09f7b48e333101ba015a3d634',1,'Aircarrier::Aircarrier(const Aircarrier &amp;ac)']]],
  ['aircover',['Aircover',['../class_aircover.html',1,'Aircover'],['../class_aircover.html#a36de878c51cf882c2540cdcad654568f',1,'Aircover::Aircover()'],['../class_aircover.html#a0cea7a6d04bca27e351f2259d7d81745',1,'Aircover::Aircover(const std::string &amp;name, Cap &amp;captain, float &amp;speed, float &amp;consumption, float &amp;fuel, int &amp;member, Ship *coveredship=nullptr)']]]
];
