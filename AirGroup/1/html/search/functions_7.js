var searchData=
[
  ['iam',['iAm',['../class_ship.html#a5a9c7ac4c426f57870d73b2a5285a3a7',1,'Ship::iAm()'],['../class_aircarrier.html#a9895fcdb86930344d5daefab87c1cf2c',1,'Aircarrier::iAm()'],['../class_covership.html#acedd0c8ca1999c216fd037c6c12677c1',1,'Covership::iAm()'],['../class_aircover.html#a57ee94c6e6ced4f5d8c8659c18a859c9',1,'Aircover::iAm()']]],
  ['insert',['insert',['../class_table.html#aab3cdf3032d5baa09afc26992f7286b4',1,'Table::insert()'],['../class_fleet.html#a7726c7bee83c264726b3ffdd8773eed8',1,'Fleet::insert(const std::string &amp;s, Ship &amp;, int=0)'],['../class_fleet.html#a961207726c7b016568495c2eed620dc6',1,'Fleet::insert(const Plane &amp;, const std::string &amp;)']]]
];
