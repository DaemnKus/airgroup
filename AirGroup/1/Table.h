/*!
\file
\brief ������������ ���� � ��������� ������-���������� � �����

�������� � ���� ����������� ������� �������, ��������� ��� �������, �����
*/

#pragma once
#ifndef _TABLE_H_
#define _TABLE_H_

#include <iostream>
#include <string>
#include "Ship.h"
#include <map>
#include <vector>

/*!
	������������� �������� ������ � ����� ��� ����
	\param [in,out] os �����, � ������� �������������� �����
	\param [in] p ����, ��������� � �����
	\return �����, � ������� ������������� �����
*/
std::ostream & operator <<(std::ostream &, const std::pair<std::string, Ship *> &);

/*!
\brief �����-�������� ��� ���������� "�������"
\author ������� �������
\version 1.0
\date ������� 2015 ����

������ ����� ������������ ������ � ��������� ���������� "�������"
*/
class TableIt {
private:
	//!�������� ���������
	std::map<const std::string, Ship *>::iterator cur;
public:
	/*!
		������ ���������������� �����������
	*/
	TableIt() {}
	/*!
		���������������� �����������
		\param [in] it �������� ��������� ������� ������ map
	*/
	TableIt(std::map<const std::string, Ship *>::iterator it) :cur(it) {}
	/*!
		������������� �������� �����������
		\param [in] t ������, � ������� ���������� ���������
		\return 0 ��� ����������, 1 ��� ������������
	*/
	int operator !=(const TableIt &) const;
	/*!
	������������� �������� ���������
	\param [in] t ������, � ������� ���������� ���������
	\return 1 ��� ����������, 0 ��� ������������
	*/
	int operator ==(const TableIt &) const;
	/*!
		������������� �������� ���������
		\return ���� "�������� - �������", �����. ������� ���������� ���������
	*/
	std::pair<const std::string, Ship *> & operator *();
	/*!
		������������� �������� ����������� ����������
		\return �������� ���������, ��������������� ���������� ������� �������
	*/
	TableIt & operator ++();
	/*!
	������������� �������� ������������ ����������
	\return �������� ���������, ��������������� ���������� ������� �������
	*/
	TableIt operator ++(int);
};

/*!
\brief ������������ ����� "�������"
\author ������� �������
\version 1.0
\date ������� 2015 ����

������ ����� ��������� ���������, ������������ STL ������ map
*/
class Table{
private:
	//! ������������� ������ �� ���
	std::map<const std::string, Ship *> arr;
public:
	/*! 
		������ ���������������� �����������
	*/
	Table(){}
	/*!
		���������� �����������
		\param [in] t ���������� �������
	*/
	Table(const Table &);
	~Table();
	/*!
		������������� �������� ������������ ��� �������
		\param [in] t ���������� �������
		\return ���������� �������
	*/
	Table& operator = (const Table &);
	/*!
		��������� ����� ������� � ���������
		\param [in] s �������� �������
		\param [in] sh ����������� �������
		\throw badins ��� ������ �������
	*/
	void insert(const std::string &, const Ship *);
	/*!
	������� ������� �� ����������
	\param [in] s �������� �������
	\throw baddel ��� ���������� ������� � ����������
	*/
	void del(const std::string &);
	/*!
		������������� �������� ��������������
		\param [in] s �������� �������
		\return ���� "��������-�������"
	*/
	const std::pair<std::string, Ship *> operator [](const std::string &) const;
	/*!
		�������� ���������� ���������� ����������, � ��� ����� � ��������
		\return ���������� ����������
	*/
	int getCnt() const;
	/*!
		�������� ������� �� ���������� �� ���������
		\param [in] s �������� �������
		\return ��������� �� ���������� �������
	*/
	Ship* getShip(const std::string &) const;
	/*!
		������������� �������� ������ � �����
		\param [in,out] os �����, � ������� �������������� �����
		\param [in] t �������, ����������� � �����
		\return �����,  ������� ������������� ����� � �����
	*/
	friend std::ostream & operator <<(std::ostream &, const Table &);
	friend class TableIt;
	typedef TableIt Iterator;
	/*!
		�������� ������ �������
		\return �������� ��������� ��� ������ �������
	*/
	Iterator begin();
	/*!
	�������� ����� �������
	\return �������� ��������� ��� ����� �������
	*/
	Iterator end();
	/*!
		���� ������� � ���������� �� ���������
		\param [in] s �������� �������
		\return �������� ���������, �������������� ���������
	*/
	Iterator find(const std::string &);
};

/*!
\brief ����� "����"
\author ������� �������
\version 1.0
\date ������� 2015 ����

������ ����� ��������� ����, ���������� ������� ������ � ������������ ������
*/
class Fleet{

private:
	//!������� ������
	std::vector<Table> esq;
	//!���������� � ����������� ������
	Cap commander;
public:
	/*!
		������ ���������������� �����������
	*/
	Fleet();
	/*!
		������������� �������� ��������������
		\param [in] s �������� �������
		\return �������, ����������� �� ����� � ��������������� ��������
		\throw badCall ��� ���������� �������
	*/
	Ship & operator [](const std::string &);
	/*!
	������������� �������� ��������������
	\param [in] s �������� �������
	\return �������, ����������� �� ����� � ��������������� ��������, �� ���������� ���������
	\throw badCall ��� ���������� �������
	*/
	const Ship & operator [](const std::string &) const;
	/*!
		�������� ������� �� ����� ��� ������
		\param [in] s �������� �������, �� ������� ��������� �������
		\param [in] k ����� ��������
		\return ���������� �������
		\throw badCall ��� ���������� �������� �������
		\throw badPlCall ��� ���������� �������� ��������
		\throw badSh ��� ������ �������, ������� �� ����� ����� �������
	*/
	const Plane &getPlane(const std::string &, int &) const;
	/*!
	�������� ������� �� ����� ��� �����������
	\param [in] s �������� �������, �� ������� ��������� �������
	\param [in] k ����� ��������
	\return ���������� �������
	\throw badCall ��� ���������� �������� �������
	\throw badPlCall ��� ���������� �������� ��������
	\throw badSh ��� ������ �������, ������� �� ����� ����� �������
	*/
	Plane &modPlane(const std::string &, int &);
	/*!
		�������� ���������� �������� ������������� ����
		\param [in] k ��� ��������
		\return ���������� ����������
	*/
	int getcount(int);
	/*!
		�������� ���������� ������ �� �����
		\return ���������� ������ �� �����
	*/
	int getesqcount() const{ return esq.size(); }
	/*!
		�������� ������� �� ������
		\param [in] i ����� �������
		\return ������� �������
	*/
	const Table &getesq(int i){ return esq[i]; }
	/*!
		�������� �������� ������������ ������
		\return �������� ������������ ������
	*/
	const Cap &getcap() const { return commander; }
	/*!
		������� ������� �� ����
		\param [in] s �������� �������
		\param [in] sh �������
		\param [in] k ����� �������
	*/
	void insert(const std::string &s, Ship &, int = 0);
	/*!
	������� �������� �� ����
	\param [in] pl �������	
	\param [in] s �������� �������, �� ������� ���������� �������
	\throw tooMuchAdd ��� ���������� �����, ��� 10 ���������
	\throw badSh ��� ������ �������, ������� �� ����� ����� �������
	\throw badCall ��� ���������� �������� �������
	*/
	void insert(const Plane &, const std::string &);
	/*!
	�������� ������� �� �����
	\param [in] s �������� �������
	*/
	void delShip(const std::string &);
	/*!
	�������� �������� �� �����
	\param [in] s �������� �������
	\param [in] k ����� �������� �� �������
	\throw badSh ��� ������ �������, ������� �� ����� ����� �������
	\throw badCall ��� ���������� �������� �������
	*/
	void delPlane(const std::string &, int &);
	/*!
		������� �������� � ������ ������� �� ������
		\param [in] dst �������� �������-���������
		\param [in] src �������� �������-���������
		\param [in] k ����� �������� �� �������-���������
		\throw badSh ��� ������ �������, ������� �� ����� ����� �������
	*/
	void transport(const std::string &dst, const std::string &src, int = 0);
	/*!
		������� ������� � ������ ������� �� ������
		\param [in] dst �������� �������-���������
		\param [in] src �������� �������-���������
		\param [in] k ���������� ������������ �������
		\throw badFuel ��� ��������� ������� �� �������-���������
	*/
	void fueltrans(const std::string &dst, const std::string &src, float = 0);
	/*!
		������ ������������ ������������ ������������� ������� ���� ��������� ��� ��������� ������� �������
		\return ������������ ������������
	*/
	float maxtime();
	/*!
	������ ������� �������� �������
	\param [in] k ����� �������
	\return ������������ ������ ��������
	*/
	float radius(int );
};

#endif
