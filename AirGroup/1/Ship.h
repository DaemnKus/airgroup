/*!
\file
\brief ������������ ���� � ��������� �������� �������

�������� � ���� ����������� ������� ������, ���������, ��������� � ��������
*/
#pragma once
#ifndef _SHIP_H_
#define _SHIP_H_
#include <iostream>
#include <string>


/*!
\brief ����� "������"
\author ������� �������
\version 1.0
\date ������� 2015 ����
	
������ ����� ��������� ���������� �������/��������
*/
class Weapon
{
private:
	//!�������� ������	
	std::string	name; 
	//!�������� ����������
	std::string	ammunition;
	//!����
	float destruction;
	//!��� ������
	int type;
	//!����������������
	int	firespeed;		
	//!���������� ��������
	int quant;				
public:
	/*!
		�������� ��� ������
		\return ���������� ��� ������
	*/
	std::string getName() const { return name; }
	/*!
		�������� �������� ����������
		\return ���������� �������� ����������
	*/
	std::string getAmm() const { return ammunition; }
	/*!
		�������� ���������������� ������
		\return ���������� �������� ����������������
	*/
	int getfspeed() const { return firespeed; }
	/*!
		�������� ��������� ������� ����
		\return ���������� ����
	*/
	float getdestr() const { return destruction; }
	/*!
		�������� ��� ������
		\return ���������� ���
	*/
	int getType() const { return type; }
	/*!
		�������� ���������� ��������
		\return ��������� ��������
	*/
	int getQuant() const { return quant; }
	/*!
		������������ ����� ��� ������
		\param [in] s ����� ���
	*/
	void setName(std::string &s) { name = s; }
	/*!
	������������ ����� ��� ����������
	\param [in] a ����� ��� ����������
	*/
	void setAmm(std::string &a) { ammunition = a; }
	/*!
	������������ ����� �������� ���������������� ������
	\param [in] s ����� �������� ����������������
	*/
	void setfspeed(int &s) { firespeed = s; }
	/*!
	������������ ����� �������� ���������� ����� �������
	\param [in] d ����� �������� �����
	*/
	void setdestr(float &d) { destruction = d; }
	/*!
	������������ ����� ���������� ��������
	\param [in] q ����� ��������� ��������
	*/
	void setQuant(int &q) { quant = q; }
	/*!
	������������ ����� ��� ������
	\param [in] t ����� ���
	*/
	void setType(int &t){ type = t; }
	/*!
	������������� �������� ������ � �����
	\param [in,out] os �����, � ������� �������������� �����
	\param [in] w ������, ��������� � �����
	\return �����, � ������� ������������� �����
	*/
	friend std::ostream & operator << (std::ostream &, const Weapon &);
	/*!
		������ ���������������� �����������
	*/
	Weapon() : name("") , ammunition(""), destruction(0),
				type(0), firespeed(0), quant(0){}
	/*!
		���������������� �����������
		\param [in] s ��� ������
		\param [in] amm �������� ����������
		\param [in] d ��������� ����
		\param [in] t ��� ������
		\param [in] f ����������������
		\param [in] q ���������� ��������
	*/
	Weapon(const std::string &s, const std::string &amm, float d = 0, int t = 0, int f = 0, int q = 0) :
		name(s), ammunition(amm), destruction(d), type(t), firespeed(f), quant(q){}
	/*!
		���������� �����������
		\param [in] w ���������� ������
	*/
	Weapon(const Weapon &w): name(w.name), ammunition(w.ammunition), destruction(w.destruction),
							type(w.type), firespeed(w.firespeed), quant(w.quant){}
};

/*!
\brief ��������� "�������� ������� ���� ����������"
\author ������� �������
\version 1.0
\date ������� 2015 ����

������ ��������� ��������� ��� ���������� ��������
*/
struct Type{
	//!��� ���
	int type;
	//!���������� ���������� ����� ����
	int curc;
	//!������ �������� ����������
	int m[5];
};

/*!
\brief ����� "�������"
\author ������� �������
\version 1.0
\date ������� 2015 ����

������ ����� ��������� �������
*/
class Plane
{
private:
	//!������������ ���������� �������� ����������
	static const int WPcount = 5;
	//!������� ���������� �������� ����������
	int curwp = 0;
	//!������� ����������
	Weapon weapon[WPcount];
	//!���� ������� ����������
	Type type[3];
	//!�������� ��������
	float speed;
	//!������ �������
	float consumption;
	//!����� �������
	float fuel;
public:
	/*!
	�������� ������� ���������� �������� ����������
	\return ���������� ���������� �������� ����������
	*/
	int getCw() const { return curwp; }
	/*!
	�������� ����������
	\param [in] k ����� ����������
	\return ���������� ����������
	*/
	const Weapon &getWeapon(int k = 0) const { return weapon[k]; }
	/*!
	�������� �������� ��������
	\return ���������� �������� �������� ��������
	*/
	float getSpeed() const { return speed; }
	/*!
	�������� ������ ������� ��������
	\return ���������� �������� ������� �������
	*/
	float getCons() const { return consumption; }
	/*!
	�������� ������� ����� ������� ��������
	\return ���������� �������� �������� ������ �������
	*/
	float getFuel() const { return fuel; }
	/*!
	�������� ������� ������� ����� ������� ����������
	\return ���������� ������� �������
	*/
	Type getType(int k = 0) const { return type[k]; }
	/*!
	������������ ����� ������� ���������� ����������
	\param [in] cw  ����� ������� ���������� ����������
	*/
	void setCw(int cw) { curwp = cw; }
	/*!
	������������ ����� �������� ��������
	\param [in] s  ����� �������� �������� ��������
	*/
	void setSpeed(float s) { speed = s; }
	/*!
	������������ ����� �������� ������� �������
	\param [in] c  ����� �������� ������� �������
	*/
	void setCons(float c) {consumption = c;}
	/*!
	������������ ����� �������� ������ �������
	\param [in] f  ����� �������� ������ �������
	*/
	void setFuel(float f) { fuel = f; }
	/*!
	������������ �����/�������� ������
	\param [in] w  ����� ������
	\param [in] k  ����� ��������������� ������
	\throw InvAdd ��� ������� ���������� 4-�� ���� ������
	\throw TooMuchAdd ��� ������� �������� �����, ��� 5 ������
	*/
	void setWeapon(Weapon &, int = 0);
	/*!
	������������ ������������ ����� ������ ��� ������� ������� ������� � ��� ������
	\return ����������� �����
	\throw cnt_err ��� ����������� ������� �� ����
	*/
	float time() const{ if (!consumption || !speed) throw std::exception("Cannot count"); 
						float d = fuel / consumption; d = d / speed; return d; }
	/*!
	������������ ������������ ��������� ������ ��� ������� ������� ������� � ��� ������
	\return ����������� ��������� ������
	\throw cnt_err ��� ����������� ������� �� ����
	*/
	float maxdistance() const {
		if (!consumption) throw std::exception("Cannot count");
								return (fuel / consumption); }
	//!\details������ ���������������� �����������
	Plane();
	/*!
	���������������� �����������
	\param [in] speed �������� ��������
	\param [in] consumption ������ �������
	\param [in] fuel ����� ������ ��������
	*/
	Plane(float speed, float consumption, float fuel) : speed(speed), consumption(consumption), fuel(fuel){
		Weapon w;
		for (int i = 0; i < WPcount; i++){
			weapon[i] = w;
		}
		for (int i = 0; i < 3; i++){
			type[i] = { 0, 0, { 0, 0, 0, 0, 0 } };
		}
	}
	/*!
	���������� �����������
	\param [in] pl ���������� �������
	*/
	Plane(const Plane&);
	/*!
		������������� �������� ������ � �����
		\param [in,out] os �����, � ������� ���������� �����
		\param [in] pl �������, ����������� � �����
		\return �����, � ������� ������������ �����
	*/
	friend std::ostream & operator <<(std::ostream &, const Plane &);
};

/*!
\brief ��������� "�������"
\author ������� �������
\version 1.0
\date ������� 2015 ����

������ ��������� ��������� ��������
*/
struct Cap{
	//!������
	std::string	rank;
	//!���
	std::string	name;
	//!�������
	std::string	srnm;
	//!��������
	std::string	midnm;
	//!����
	int	exp;
	/*!
		������������� �������� ������ � ����� ��� ��������
		\param [in,out] os �����, � ������� �������������� �����
		\param [in] c ������, ����������� � �����
		\return �����, � ������� ������������� �����
	*/
	friend std::ostream & operator <<(std::ostream &os, const Cap &c);
};


/*!
\brief ����������� ����� "�������"
\author ������� �������
\version 1.0
\date ������� 2015 ����

������ ����� ��������� �������
*/
class Ship
{
private:
	//!��� �������
	std::string name;
	//!���������� � ��������
	Cap captain;
	//!�������� ������� 
	float speed;
	//!������ �������
	float consumption;
	//!����� �������
	float fuel;
	//!���������� ������ �������
	int member;
	//!������������ ���������� ����� ����������
	static const int WPcount = 25;
	//!������� ���������� ����� ����������
	int curwp = 0;
	//!����������
	Weapon weapon[WPcount];
public:
	/*!
		������ ���������������� �����������
	*/
	Ship();
	/*!
		���������������� �����������
		\param [in] name ��� �������
		\param [in] captain �������
		\param [in] speed �������� �������
		\param [in] consumption ������ �������
		\param [in] fuel ����� �������
		\param [in] member ���������� ������ �������
	*/
	Ship(const std::string &name, Cap &captain, float &speed, float &consumption, float &fuel, int &member) :
		name(name), captain(captain), speed(speed), consumption(consumption), fuel(fuel), member(member){
		Weapon w;
		for (int i = 0; i < WPcount; i++){
			weapon[i] = w;
		}
	}
	/*!
		���������� �����������
		\param [in] sh ���������� �������
	*/
	Ship(const Ship&);
	virtual ~Ship();
	/*!
	�������� ��� �������
	\return ���������� ��� �������
	*/
	std::string getName() const;
	/*!
		������������� ����� ��� �������
		\param [in] s ����� ��� �������
	*/
	void setName(std::string &);
	/*!
	�������� �������� ��������
	\return ���������� �������� ��������
	*/
	Cap getCap() const;
	/*!
		������������� ������ ��������
		\param [in] c ����� �������
	*/
	void setCap(Cap &);
	/*!
	�������� �������� �������
	\return ���������� �������� �������
	*/
	float getSpeed() const;
	/*!
	�������� ������ ������� �������
	\return ���������� �������� ������� �������
	*/
	float getCons() const;
	/*!
	�������� ����� ������� �������
	\return ���������� �������� ������ �������
	*/
	float getFuel() const;
	/*!
	������������� ����� �������� �������
	\param [in] s ����� ��������
	*/
	void setSpeed(float &);
	/*!
	������������� ����� ������ ������� �������
	\param [in] cons ����� �������� ������� �������
	*/
	void setCons(float &);
	/*!
	������������� ����� �������� ������� ������� �������
	\param [in] f ����� �������� ������� �������
	*/
	void setFuel(float &);
	/*!
	������������� ����� ���������� ������ �������
	\param [in] m ����� �������� ���������� ������ �������
	*/
	void setMemb(int &);
	/*!
	�������� ���������� ������ �������
	\return ���������� ���������� ������ �������
	*/
	int getMemb() const;
	/*!
	�������� ������� ���������� ������ �������
	\return ���������� ���������� ������
	*/
	int getCw() const;
	/*!
		������������ ������������ ��������� �������� ��� ������� ������� � ������ �������
		\return ������������ ���������
		\throw badCnt ��� ����������� ������� �� ����
	*/
	float maxdistance() const;
	/*!
		�������� ����������
		\param [in] k ����� ���������� � �������
		\return ���������� ����������
	*/
	const Weapon &getWeapon(int k = 0) const;
	/*!
		��������/������������� ����� ����������
		\param [in] wp ��������������� ���������� 
		\param [in] k ����� ���������������� ����������
		\throw tooMuchAdd ��� ���������� �����, ��� 25 ���������� 
	*/
	void setWeapon(Weapon &, int = 0);
	/*!
		������������ ������������ ����� ������� ���� �� ���� ����������
		\return ������������ �����
	*/
	float firetime() const;
	/*!
		������������� ������� ���������� ����������
		\param [in] cw ����� ���������� ����������
	*/
	void setCw(int cw) { curwp = cw; }
	/*!
		��������� ������
		\return ��������� �� ����� ������
	*/
	virtual Ship *clone() const = 0;
	/*!
		���������� ��� �������
		\return ��� �������
	*/
	virtual int iAm() const = 0;
	/*!
		������������� �������� ������ � �����
		\param [in,out] os �����, � ������� ������������ �����
		\param [in] sh �������, ��������� � �����
		\return �����, � ������� ������������ �����
	*/
	friend std::ostream & operator <<(std::ostream &, const Ship &);
protected:
	/*!
		��������������� ������� ��� ������������ ������ � �����
		\param [in,out] os �����, � ������� ������������ �����
		\return �����, � ������� ������������ �����
	*/
	virtual std::ostream & print(std::ostream &os) const = 0;
};

/*!
\brief ����� "���������"
\author ������� �������
\version 1.0
\date ������� 2015 ����

������ ����� ��������� ���������
*/
class Aircarrier :
	virtual public Ship
{
private:
	//!���������� ���������
	static const int PLcount = 10;
	//!�������� ���������
	Plane planes[PLcount];
public:
	/*!
		������ ���������������� ����������
	*/
	Aircarrier();
	/*!
		���������������� �����������
		\param [in] name ��� ���������
		\param [in] captain ������� ���������
		\param [in] speed �������� ���������
		\param [in] consumption ������ ������� ���������
		\param [in] fuel ����� ������� ���������
		\param [in] member ���������� ������ ���������
	*/
	Aircarrier(const std::string &name, Cap &captain, float &speed, float &consumption, float &fuel, int &member) :
		Ship(name, captain, speed, consumption, fuel, member){
		Plane p;
		for (int i = 0; i < PLcount; i++){
			planes[i] = p;
		}
	}
	/*!
		���������� �����������
		\param [in] ac ���������� ���������
	*/
	Aircarrier(const Aircarrier &ac) :Ship(ac), curpl(ac.curpl){
		for (int i = 0; i < PLcount; i++){
			planes[i] = ac.planes[i];
		}
	}
	/*!
		�������� ������� ���������
		\param [in] k ����� ���������
		\return ���������� ������� ���������
	*/
	const Plane &getPlane(int k = 0) const{ return planes[k]; }
	/*!
		�������� ������� ��������� ��� �����������
		\param [in] k ����� ���������
		\return ���������� c������ ���������
	*/
	Plane &modPlane(int k){ return planes[k]; }
	/*!
		�������� ������� ���������� ���������
		\return ���������� ����������
	*/
	int getpl() const{ return curpl; }
	/*!
		������������� ����� ���������� ���������
		\param [in] p ����� ���������� ���������
	*/
	void setpl(int p){ curpl = p; }
	/*!
	���������/�������� �������
	\param [in] pl ����� �������
	\param [in] n ����� ��������
	\throw tooMuchAdd ��� ���������� �����, ��� 10 ���������
	*/
	void setPlane(Plane, int = 9);
	/*!
		������������ ����������, ��������� ����� ���������� ����� �� ������� �������
		\return ������������ ����
	*/
	float countpldestruction() const;
	virtual Ship *clone() const { return new Aircarrier(*this); };
	/*!
	������������� �������� ������ � �����
	\param [in,out] os �����, � ������� ������������ �����
	\param [in] ac ���������, ��������� � �����
	\return �����, � ������� ������������ �����
	*/
	friend std::ostream & operator <<(std::ostream &, const Aircarrier &);
	virtual int iAm() const{ return 1; }
protected:
	//!������� ���������� ��������� �� �������
	int curpl = 0;
	virtual std::ostream & print(std::ostream &) const;
};

/*!
\brief ����� "����� ��������"
\author ������� �������
\version 1.0
\date ������� 2015 ����

������ ����� ��������� ����� ���������
*/
class Covership :		
	virtual public Ship
{
private:
	//!������������ �������
	Ship *coveringship;
public:
	/*!
		������ ���������������� �����������
	*/
	Covership();
	/*!
		���������������� �����������
		\param [in] name ��� ����� ��������
		\param [in] captain ������� �����
		\param [in] speed �������
		\param [in] consumption ������ �������
		\param [in] fuel ����� �������
		\param [in] member ���������� ������
		\param [in] coveredship ������������ �������
	*/
	Covership(const std::string &name, Cap &captain, float &speed, float &consumption, float &fuel, int &member, Ship * coveredship = nullptr) :
		Ship(name, captain, speed, consumption, fuel, member), coveringship(coveredship){}
	/*!
		���������� �����������
		\param [in] cs ���������� ����� ���������
	*/
	Covership(const Covership &cs) : Ship(cs), coveringship(cs.coveringship){}
	/*!
		�������� ������������ �������
		\return ��������� �� ������������ �������
	*/
	const Ship *getcoveringship() const;
	/*!
		������������� ����� ������������ �������
		\param [in] sh ��������� �� ����� ������������ �������
	*/
	void setcoveringship(Ship *);
	/*!
		������������ ����������, ��������� ���� ����������� ����� � ������� �������
		\return ������������ ����
	*/
	float countdestruction() const;
	virtual Ship *clone() const { return new Covership(*this); };
	/*!
	������������� �������� ������ � �����
	\param [in,out] os �����, � ������� ������������ �����
	\param [in] ac ����� ���������, ��������� � �����
	\return �����, � ������� ������������ �����
	*/
	friend std::ostream & operator <<(std::ostream &, const Covership &);
	virtual int iAm() const { return 2; }
protected:
	virtual std::ostream & print(std::ostream &) const;
};

/*!
\brief ����� "����������� �������"
\author ������� �������
\version 1.0
\date ������� 2015 ����

������ ����� ��������� ����������� �������
*/
class Aircover :
	public Aircarrier,
	public Covership
{
public:
	/*!
		������ ���������������� �����������
	*/
	Aircover() :Aircarrier(), Covership(), Ship(){}
	/*! 
		���������������� �����������
		\param [in] name ��� ������������ ��������
		\param [in] captain �������
		\param [in] speed ��������
		\param [in] consumption ������ �������
		\param [in] fuel ����� �������
		\param [in] member ���������� ������ �������
		\param [in] coveredship ������������ �������
	*/
	Aircover(const std::string &name, Cap &captain, float &speed, float &consumption, float &fuel, int &member, Ship * coveredship = nullptr) :
		Aircarrier(), Covership(), Ship(name, captain, speed, consumption, fuel, member){
		setcoveringship(coveredship);
	}
	virtual Ship *clone() const { return new Aircover(*this); };
	/*!
	������������� �������� ������ � �����
	\param [in,out] os �����, � ������� ������������ �����
	\param [in] ac ����������� �������, ��������� � �����
	\return �����, � ������� ������������ �����
	*/
	friend std::ostream & operator <<(std::ostream &, const Aircover &);
	virtual int iAm() const { return 3; }
protected:
	virtual std::ostream & print(std::ostream &) const;
};


#endif