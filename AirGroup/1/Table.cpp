#include "stdafx.h"
#include "Table.h"

std::ostream & operator <<(std::ostream &os, const std::pair<std::string, Ship *> &p){
	os << '"' << p.first << '"';
	if (p.second)
		os << " - " << (*p.second);
	else
		os << "is empty";
	return os;
}

Table::Table(const Table &a){
	std::map<const std::string, Ship *>::const_iterator p;
	for (p = a.arr.begin();	p != a.arr.end(); ++p)
		arr.insert(std::make_pair(p->first, p->second->clone()));
}

Table::~Table(){
	std::map<const std::string, Ship *>::iterator p;
	for (p = arr.begin(); p != arr.end(); ++p){ 
		delete p->second; 
		p->second = nullptr;
	}
}

Table& Table::operator = (const Table &a){
	std::map<const std::string, Ship *>::iterator p;
	if (this != &a){
		for (p = arr.begin(); p != arr.end(); ++p)
			delete p->second;
		arr.clear();
		std::map<const std::string, Ship *>::const_iterator pp;
		for (pp = a.arr.begin(); pp != a.arr.end(); ++pp)
			arr.insert(make_pair(pp->first, pp->second->clone()));
	}
	return *this;
}

void Table::insert(const std::string &s, const Ship *f){
	std::map<const std::string, Ship *>::iterator p = arr.find(s);
	if (p == arr.end()){
		std::pair<std::map<const std::string, Ship *>::iterator, bool> pp = arr.insert(make_pair(s, f->clone()));
		if (!pp.second)
			throw std::exception("can't insert new item into map");
	}
	else{
		delete p->second;
		p->second = f->clone();
	}
}

void Table::del(const std::string &s){
	Table::Iterator it = find(s);
	if (it == end()){
		throw std::exception("This ship is absent");
	}
	arr.erase(s);
}

const std::pair<std::string, Ship *> Table::operator [ ](const std::string &s) const{
	std::pair<std::string, Ship *> res;
	std::map<const std::string, Ship *>::const_iterator p = arr.find(s);
	return (p == arr.end()) ? res : (*p);
}

Ship* Table::getShip(const std::string &s) const{
	std::map<const std::string, Ship *>::const_iterator p;
	p = arr.find(s);
	if (p == arr.end())
		return nullptr;
	return (*p).second;
}

int Table::getCnt() const{
	int cnt = arr.size();
	std::map<const std::string, Ship *>::const_iterator p;
	for (p = arr.begin(); p != arr.end(); p++){
		int k = (*p).second->iAm();
		if (k == 1){
			Aircarrier *ac = dynamic_cast<Aircarrier *>((*p).second);
			cnt += ac->getpl();
			continue;
		}
		if (k == 3){
			Aircover *ac = dynamic_cast<Aircover *>((*p).second);
			cnt += ac->getpl();
			continue;
		}
	}
	return cnt;
}

std::ostream & operator <<(std::ostream &os, const Table &t){
	std::map<const std::string, Ship *>::const_iterator p;

	for (p = t.arr.begin(); p != t.arr.end(); p++){
		os << (*p);
	}
	return os;
}

Table::Iterator Table::find(const std::string &s) {
	std::map<const std::string, Ship *>::iterator p = arr.find(s);
	return TableIt(p);
}

Table::Iterator Table::begin(){
	return TableIt(arr.begin());
}

Table::Iterator Table::end(){
	return TableIt(arr.end());
}


int Table::Iterator::operator !=(const TableIt &it) const{
	return cur != it.cur;
}

int TableIt::operator ==(const TableIt &it) const{
	return cur == it.cur;
}

std::pair<const std::string, Ship *> & TableIt::operator *(){
	return *cur;
}

TableIt & TableIt::operator ++(){
	++cur;
	return *this;
}

TableIt TableIt::operator ++(int) {
	TableIt res(*this);
	++cur;
	return res;
}

Fleet::Fleet()
{
	commander = { "Captain", "Ivan", "Ivanov", "Ivanovich", "7 years" };
}

Ship & Fleet::operator [](const std::string &s){
	Table::Iterator it;
	std::string err = "\""+s+"\" - Ship with this sign is absent";
	for (size_t i = 0; i < esq.size(); i++){
		Ship *sh = (esq[i]).getShip(s);
		if (sh)
			return *(sh);
	}
	throw std::exception(err.c_str());
}

const Ship & Fleet::operator [](const std::string &s) const{
	Table::Iterator it;
	std::string err = "\"" + s + "\" - Ship with this sign is absent";
	for (size_t i = 0; i < esq.size(); i++){
		Ship *sh = (esq[i]).getShip(s);
		if (sh)
			return *(sh);
	}
	throw std::exception(err.c_str());
}


const Plane &Fleet::getPlane(const std::string &s, int &k) const {
	const Ship *sh = &((*this)[s]);
	if (sh){
		if (sh->iAm() == 1){
			const Aircarrier *ac = dynamic_cast<const Aircarrier *>(sh);
			if (k < ac->getpl()){
				return ac->getPlane(k);
			}
			else
				throw std::exception("This plane is absent");
		}
		if (sh->iAm() == 3){
			const Aircover *ac = dynamic_cast<const Aircover *>(sh);
			if (k < ac->getpl()){
				return ac->getPlane(k);
			}
			else
				throw std::exception("This plane is absent");
		}
		throw std::exception("This ship has no any plane");
	}
	else throw std::exception("The ship with this sign doesn't exist");
}

Plane & Fleet::modPlane(const std::string &s, int &k){
	Ship *sh = &((*this)[s]);
	if (sh){
		if (sh->iAm() == 1){
			Aircarrier *ac = dynamic_cast<Aircarrier *>(sh);
			if (k < ac->getpl()){
				return ac->modPlane(k);
			}
			else
				throw std::exception("This plane is absent");
		}
		if (sh->iAm() == 3){
			Aircover *ac = dynamic_cast<Aircover *>(sh);
			if (k < ac->getpl()){
				return ac->modPlane(k);
			}
			else
				throw std::exception("This plane is absent");
		}
		throw std::exception("This ship has no any plane");
	}
	else throw std::exception("The ship with this sign doesn't exist");
}

int Fleet::getcount(int t){
	int cnt = 0;
	Table::Iterator it;
	for (size_t i = 0; i < esq.size(); i++){
		for (it = esq[i].begin(); it != esq[i].end(); it++){
			if ((*it).second->iAm() == t)
				cnt++;
		}
	}
	return cnt;
}

void Fleet::insert(const std::string &s, Ship &sh, int k){
	int sz = esq.size();
	if (k < sz){		
		esq[k].insert(s, &sh);
	}
	else{
		Table t;
		std::vector<Table>::iterator p = esq.end();
		esq.insert(p, t);
		esq[esq.size() - 1].insert(s, &sh);
	} //throw std::exception("Esquadr with this number doesn't exist");
}

void Fleet::insert(const Plane &pl, const std::string &s){
	Ship *sh = &((*this)[s]);
	if(sh){
		if (sh->iAm() == 1){
			Aircarrier *ac = dynamic_cast<Aircarrier *>(sh);
			if (ac->getpl() < 10){
				ac->setPlane(pl);
				return;
			}
			else
				throw std::exception("Cannot add more than 10 planes");
		}
		if (sh->iAm() == 3){
			Aircover *ac = dynamic_cast<Aircover *>(sh);
			if (ac->getpl() < 10){
				ac->setPlane(pl);
				return;
			}
			else
				throw std::exception("Cannot add more than 10 planes");
		}
		throw std::exception("This ship cannot have any plane");
	}
	else throw std::exception("The ship with this sign doesn't exist");
}

void Fleet::delShip(const std::string &s){
	Table::Iterator it;
	int k;
	for (size_t i = 0; i < esq.size(); i++){
		Ship *sh1 = (esq[i]).getShip(s);
		if (sh1){
			it = esq[i].find(s);
			k = i;
			break;
		}
	}
	esq[k].del(s);
	it = esq[k].begin();
	int i = 0;
	for (; it != (esq[k]).end(); it++)
		i++;	
	if (!i){
		std::vector<Table>::iterator p = esq.begin();
		while (&(*p) != &esq[k] || p != esq.end())
			++p;
		esq.erase(p);
	}
}

void Fleet::delPlane(const std::string &s, int &k){
	Ship *sh = &((*this)[s]);
	if (!sh){
		throw std::exception("Ship doesn't exist");
	}
	if (sh->iAm() == 2)
		throw std::exception("ship cannot have any plane");
	Aircarrier *ac = dynamic_cast<Aircarrier *>(sh);
	int p = ac->getpl()-1;
	for (int i = k; i < p; i++){
		ac->setPlane(ac->getPlane(k + 1), k);
	}
	Plane pl;
	ac->setPlane(pl, p);
	ac->setpl(p);
}

void Fleet::transport(const std::string &dst, const std::string &src, int k){
	Ship *sh1 = &((*this)[src]);
	Ship *sh2 = &((*this)[dst]);
	if (sh1->iAm() == 2)
		throw std::exception("Source ship cannot have any plane!");
	if (sh2->iAm() == 2)
		throw std::exception("Destination ship cannot have any plane!");
	insert(getPlane(src, k), dst);
	delPlane(src, k);
}

void Fleet::fueltrans(const std::string &dst, const std::string &src, float f){
	Ship *sh1 = &((*this)[src]);
	Ship *sh2 = &((*this)[dst]);
	float f1 = sh1->getFuel(), f2 = sh2->getFuel();
	if (f1 >= f){
		f1 -= f;
		f2 += f;
		sh2->setFuel(f2);
		sh1->setFuel(f1);
	}
	else throw std::exception("Have no enough fuel");
}

float Fleet::maxtime(){
	int sz = esq.size();
	Table::Iterator it;
	float d = 0;
	for (int i = 0; i < sz; i++){
		for (it = esq[i].begin(); it != esq[i].end(); it++){
			Ship *sh = (*it).second;
			if (sh->iAm() != 2){
				Aircarrier *ac = dynamic_cast<Aircarrier *>(sh);
				for (int j = 0; j < ac->getpl(); j++){
					float t = ac->getPlane(i).time();
					if (t>d)
						d = t;
				}
			}
		}
	}
	return d;
}

float Fleet::radius(int k){
	float rad = 0;
	//std::map<const std::string, Ship *>::iterator p = arr.find(s);
	int sz = esq.size();
	if (sz < k)
		return 0;
	Table::Iterator it;
	for (it = esq[k].begin(); it != esq[k].end(); it++){
		if ((*it).second->iAm() == 1 || (*it).second->iAm() == 3){
			Aircarrier *ac = dynamic_cast<Aircarrier *>((*it).second);
			float r1 = ac->maxdistance(), r2 = 0;
			for (int i = 0; i < ac->getpl(); i++){
				float r3 = ac->getPlane(i).maxdistance();
				if (r3>r2)
					r2 = r3;
			}
			r1 += r2;
			if (r1 > rad)
				rad = r1;
		}
	}
	return rad;
}