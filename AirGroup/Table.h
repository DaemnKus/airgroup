#pragma once
#ifndef _TABLE_H_
#define _TABLE_H_

#include <iostream>
#include <string>
#include "Ship.h"
//#include <map>
//#include <vector>
/*
class Table{
private:
	//static const int QUOTA = 10;
	int cnt;
	Pair *array;
	int maxsz;
public:
	Table();
	Table(const Table&);
	Table(Table &&);
	~Table(){ delete[] array; }
	Table & operator = (const Table &);
	Table & operator =(Table &&);
	Pair & operator [](const std::string &);
	const Pair operator [](const std::string &) const;
	int getCnt() const;
	const Ship* operator ()(const std::string &);
	friend ostream & operator <<(ostream &, const Table &);
	friend class TableIt;
	typedef TableIt Iterator;
	Iterator begin();
	Iterator end();
};

class TableIt {
private:
	Table *pTab;
	int cur;
public:
	TableIt(Table &as, int last = 0) :
		pTab(&as), cur(last){ }
	TableIt() :pTab(nullptr), cur(0){ }
	const Pair operator *() const;
	TableIt & operator ++();
	const TableIt operator ++(int);
	int operator !=(const TableIt &) const;
};
*/
std::ostream & operator <<(std::ostream &, const std::pair<std::string, Ship *> &);

template <class sign, class data>
struct Pair {
	sign first;
	data *second;
	Pair() :first(sign()), second(nullptr){ }
	Pair(sign name, const data &s) : first(name), second(s.clone()){ }
	Pair(const Pair<sign, data> &p) :first(p.first)
	{
		second = p.second ? p.second->clone() : nullptr;
	}
	Pair(Pair &&p) : first(p.first)
	{
		second = p.second;
		p.second = nullptr;
	}
	~Pair(){ delete second; }
	Pair<sign,data> & operator =(const Pair<sign, data> &);
	Pair<sign, data> & operator = (Pair<sign,data> &&);
	bool isNull()const
	{
		return /*first.empty() &&*/ second == nullptr;
	}
};

template <class sign, class data>
class TableIt{
private:
	Pair <sign, data> *cur;
public:
	TableIt(Pair<sign, data> *as) :
		cur(as){ }
	TableIt() :cur(nullptr){ }
	//TableIt(const TableIt<sign, data> &t) :cur(t.cur){}
	Pair<sign, data> & operator *();
	TableIt<sign, data> & operator ++(){
		++cur;
		return *this;
	}
	TableIt<sign, data> operator ++(int){
		TableIt<sign, data> res(*this);
		++cur;
		return res;
	}
	int operator !=(const TableIt<sign, data> &) const;
	int operator ==(const TableIt<sign, data> &) const;
};

template <class sign, class data>
class const_TableIt{
private:
	const Pair <sign, data> *cur;
	
public:
	const_TableIt(const Pair<sign, data> *as) :
		cur(as){ }
	const_TableIt() :cur(nullptr){ }
	//const_TableIt(const const_TableIt<sign, data> &t) :cur(t.cur){}
	const Pair<sign, data> & operator *(){ return *cur; }
	const_TableIt<sign, data> & operator ++(){
		++cur;
		
		return *this;
	}
	const_TableIt<sign, data> operator ++(int){
		const_TableIt<sign, data> res(*this);
		++cur;
		
		return res;
	}
	int operator !=(const const_TableIt<sign, data> &t) const{ return cur != t.cur; }
	int operator ==(const const_TableIt<sign, data> &t) const{ return cur == t.cur; }
};


template <class sign, class data>
class Table{
private:
	//std::map<const std::string, Ship *> arr;
	int cnt;
	Pair<sign,data> *arr;
	int maxsz;
public:
	Table():cnt(0), maxsz(0), arr(nullptr){}
	Table(const Table<sign, data> &a) :
		cnt(a.cnt), maxsz(a.maxsz)
	{
		arr = new Pair<sign, data>[a.cnt];
		for (int i = 0; i < cnt; i++)
			arr[i] = a.arr[i];
	}
	Table(Table<sign, data> &&a) :
		cnt(a.cnt), maxsz(a.maxsz), arr(a.arr){
		a.arr = nullptr;
	}
	~Table(){ delete[]arr; }
	Table<sign,data> & operator = (const Table<sign, data> &);
	Table<sign, data> & operator = (Table<sign, data> &&);
	void insert(const sign &, const data *);
	void del(const sign &);
	Pair <sign, data> & operator [](const sign &);
	const Pair<sign, data> operator [](const sign &)const;
	int getCnt() const;
	const int size() const{ return cnt; }
	data* getShip(const sign &) const;
	friend std::ostream & operator <<(std::ostream &, const Table<sign,data> &);
	std::ostream & print(std::ostream &) const;
	friend class TableIt<sign,data>;
	typedef TableIt<sign, data> Iterator;
	typedef const_TableIt<sign, data> const_Iterator;

	//friend class Iterator;
	Table<sign, data> *clone() const { return new Table<sign, data>(*this); }
	Iterator begin(){ return TableIt<sign,data>(arr); }	
	const_Iterator begin() const{ return const_TableIt<sign, data>(arr); }

	Iterator end(){ return TableIt<sign, data>(arr + cnt); }
	const_Iterator end() const { return const_TableIt<sign, data>(arr + cnt); }
	Iterator find(const sign &s){
		Pair<sign, data> *p = nullptr;
		for (int i = 0; i < cnt; i++){
			if (arr[i].first == s)
			{
				p = &(arr[i]);
				break;
			}
		}
		return TableIt<sign, data>(p);
	}
};
/*
template <class sign, class data>
class TableIt{
private: 
	Pair <sign, data> *cur;
public: 
	TableIt(const Pair<sign, data> *as) :
		cur(as){ }
	TableIt() :cur(nullptr){ }
	Pair<sign, data> & operator *();
	TableIt <sign, data>& operator ++();
	TableIt<sign, data> operator ++(int);
	int operator !=(const TableIt<sign, data> &) const;
	int operator ==(const TableIt<sign, data> &) const;
};*/

template <class e_sign, class s_sign>
class Fleet{
private:	
	Table<e_sign, Table<s_sign, Ship>> esq;	//������� ������
	Cap commander;		//���������� � ����������� ������
public:
	/*Fleet(Cap cp = { "Captain", "Ivan", "Ivanov", "Ivanovich", 7 }) :commander(cp){
		Table<e_sign, Table<s_sign, Ship>> t;
		esq = t;
	}*/
	
	Ship & operator [](const s_sign &);
	const Ship & operator [](const s_sign &) const;
	const Plane &getPlane(const s_sign &, int &) const;
	Plane &modPlane(const s_sign &, int &);
	int getcount(int t);
	int getesqcount() const; 
	const Table <s_sign, Ship> &getesq(const e_sign &);
	const Table <s_sign, Ship> &nesq(const int &) const;
	const Cap &getcap() const { return commander; }
	void insert(const s_sign &s, Ship &, const e_sign & = 0);
	void insert(const Plane &, const s_sign &);
	void delShip(const s_sign &);
	void delPlane(const s_sign &, int &);
	//Ship * operator ()(const string &);
	//Plane * operator ()(const string &, int &);
	void transport(const s_sign &dst, const s_sign &src, int = 0);
	void fueltrans(const s_sign &dst, const s_sign &src, float = 0);
	float	maxtime(),
			radius(e_sign );
};
//#include "Table.cpp"
#endif
