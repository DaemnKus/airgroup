#include "stdafx.h"
#include "Ship.h"

std::ostream & operator <<(std::ostream &os, const Weapon &w){
	os << "Weapon: " << w.name << "\nAmmunition: " << w.ammunition << "\nAmmo quantity: " << w.quant << "\nFirespeed: " << w.firespeed << "\nDestruction: " << w.destruction << "\nType: " << w.type << std::endl;
	return os;
}


Plane::Plane() : curwp(0), speed(0), consumption(0), fuel(0)
{ 
	Weapon w;
	for (int i = 0; i < WPcount; i++){
		weapon[i] = w;
	}
	for (int i = 0; i < 3; i++){
		type[i] = { 0, 0, {0,0,0,0,0} };
	}
}

Plane::Plane(const Plane &p): curwp(p.curwp), speed(p.speed), consumption(p.consumption), fuel(p.fuel){
	for (int i = 0; i < WPcount; i++){
		weapon[i] = p.weapon[i];
	}
	for (int i = 0; i < 3; i++){
		type[i].type = p.type[i].type;
		type[i].curc = p.type[i].curc;
		for (int j = 0; j < WPcount; j++)
			type[i].m[j] = p.type[i].m[j];
	}
}

void Plane::setWeapon(Weapon &w, int k){
	if (k < WPcount){
		int t = w.getType();
		if (t == weapon[k].getType())
		{
			weapon[k] = w;
			return;
		}
		else{
			int t1 = weapon[k].getType();

			for (int i = 0; i < 3; i++){
				if (t1 == type[i].type){
					if (type[i].curc == 1){
						weapon[k] = w;
						type[i].type = t;
						return;
					}
					else{
						for (int j = 0; j < 3; j++){
							if ((t == type[j].type) || !type[j].type){
								int p = type[i].curc-1;
								for (int s = k; s < p;){
									type[i].m[s] = type[i].m[s++];
								}
								if (t1)
									type[i].m[--(type[i].curc)] = 0;
								if (k < curwp){
									type[j].m[(type[j].curc)++] = k;
									weapon[k] = w;
									
								}
								else{
									type[j].m[(type[j].curc)++] = curwp;
									weapon[curwp++] = w;
								}
								if (!type[j].type)
									type[j].type = w.getType();
								return;
							}
						}
						throw std::exception("Cannot set weapon:You try to add the 4th type of weapon!");
					}
				}				
			}
		}
	}
	else throw std::exception("Cannot add more than 5 weapons");
}

//Plane::~Plane() {}

std::ostream & operator <<(std::ostream &os, const Plane &pl){
	os << "Weapon info:";
	for (int i = 0; i < pl.curwp; i++){
		os << "\n�" << (i + 1) << std::endl << (pl.weapon)[i];
	}
	os << "\nSpeed: " << pl.speed << "\nConsumption:" << pl.consumption << "\nFuel:" << pl.fuel << std::endl;
	return os;
}

std::ostream & operator <<(std::ostream &os, const Cap &c){
	os << "Captain \n\t" << c.srnm << " " << c.name << " " << c.midnm << std::endl;
	os << "\tRank: " << c.rank << std::endl << "\tExperience: " << c.exp << " years"<< std::endl;
	return os;
}

Ship::Ship() : name(""), speed(0), consumption(0), fuel(0), member(0){
	Weapon w;
	for (int i = 0; i < WPcount; i++){
		weapon[i] = w;
	}
	captain = {"","","","",0};
}

Ship::Ship(const Ship &sh) : name(sh.name), captain(sh.captain), speed(sh.speed), consumption(sh.consumption),
		fuel(sh.fuel), member(sh.member), curwp(sh.curwp) {
	for (int i = 0; i < WPcount; i++){
		weapon[i] = sh.weapon[i];
	}
}

std::string Ship::getName() const{ return name; }
void Ship::setName(std::string &s){ name = s; }
Cap Ship::getCap() const{ return captain;}
void Ship::setCap(Cap &c){ captain = c;}
float Ship::getSpeed() const{ return speed; }
float Ship::getCons() const{ return consumption; }
float Ship::getFuel() const{ return fuel; }
void Ship::setSpeed(float &v){ speed = v;}
void Ship::setCons(float &c){ consumption = c;}
void Ship::setFuel(float &f){ fuel = f;}
void Ship::setMemb(int &m){	member = m;}
int Ship::getMemb() const{ return member; }
int Ship::getCw() const{ return curwp; }

float Ship::maxdistance() const{
	if (!consumption) throw std::exception("Cannot count");
	return (fuel / consumption);
}

const Weapon & Ship::getWeapon(int k) const{
	return weapon[k];
}

void Ship::setWeapon(Weapon &w, int k){
	if (k < WPcount){
		if (k < curwp)
			weapon[k] = w;
		else
			weapon[curwp++] = w;
	}
	else throw std::exception("Cannot add more than 25 weapons");
}

float Ship::firetime() const{
	float max = 0;
	if (curwp){
		max = (weapon[0].getQuant() / weapon[0].getfspeed());
		for (int i = 1; i < curwp; i++){
			float p = (weapon[i].getQuant() / weapon[i].getfspeed());
			if (p>max)
				max = p;
		}
	}
	return max;
}

std::ostream & operator <<(std::ostream &os, const Ship &s){
	return s.print(os);
}

Ship::~Ship() {}




Aircarrier::Aircarrier() : Ship(){
	Plane p;
	for (int i = 0; i < PLcount; i++){
		planes[i] = p;
	}
}

Aircarrier::~Aircarrier()
{
}

void Aircarrier::setPlane(Plane pl, int k){
	if (k < PLcount){
		if (k < curpl)
			planes[k] = pl;
		else
			planes[curpl++] = pl;
	}
	else throw std::exception("Cannot add more than 10 planes");
}

std::ostream & operator <<(std::ostream &os, const Aircarrier &s){
	return s.print(os);
}

std::ostream & Aircarrier::print(std::ostream &os) const{
	os << "Type of ship: Aircarrier\nName of ship: " << getName() << std::endl << getCap() << "\nSpeed: " << getSpeed();
	os << "\nConsumption: "<<getCons()<<"\nFuel: "<<getFuel()<<"\nNumber of members: "<< getMemb()<<"\nWeapon info:";
	for (int i = 0; i < getCw(); i++){
		Weapon w;
		w = getWeapon(i);
		os << "\n�" << (i + 1) << std::endl << w;
	}
	os << "Planes info:";
	for (int i = 0; i < curpl; i++){
		os << "\nPlane �" << (i + 1) << std::endl << planes[i];
	}
	return os;
}

float Aircarrier::countpldestruction() const{
	float dest = 0;
	for (int i = 0; i < curpl; i++){
		int n = planes[i].getCw();
		for (int j = 0; j < n; j++){
			int fs = planes[i].getWeapon(j).getfspeed();
			float ds = planes[i].getWeapon(j).getdestr();
			dest += (fs*ds);
		}
	}
	return dest;
}



Covership::Covership() : Ship(), coveringship(nullptr){}

std::ostream & Covership::print(std::ostream &os) const{
	os << "Type of ship: Covership\nName of ship: " << getName() << std::endl << getCap() << "\nSpeed: " << getSpeed();
	os << "\nConsumption" << getCons() << "\nFuel" << getFuel() << "\nNumber of members: " << getMemb() << "\nWeapon info:";
	for (int i = 0; i < getCw(); i++){
		Weapon w;
		w = getWeapon(i);
		os << "\n�" << (i + 1) << std::endl << w;
	}
	if (coveringship)
		os << "Covering Ship" << (*coveringship) << std::endl;
	else
		os << "Covering Ship is absent\n";
	return os;
}

std::ostream & operator <<(std::ostream &os, const Covership &s){
	return s.print(os);
}

void Covership::setcoveringship(Ship *cs){
	coveringship = cs;
}

const Ship * Covership::getcoveringship() const{
	return coveringship;
}

float Covership::countdestruction() const{
	float dest = 0;
	for (int j = 0; j < getCw(); j++){
		Weapon w;
		w = getWeapon(j);
		int fs = w.getfspeed();
		float ds = w.getdestr();
		dest += (fs*ds);
	}
	return dest;
}

Covership::~Covership()
{
}



std::ostream & Aircover::print(std::ostream &os) const{
	os << "Type of ship: Aircover\nName of ship: " << getName() << std::endl << getCap() << "\nSpeed: " << getSpeed();
	os << "\nConsumption: " << getCons() << "\nFuel: " << getFuel() << "\nNumber of members: " << getMemb() << "\nWeapon info:";
	for (int i = 0; i < getCw(); i++){
		Weapon w;
		w = getWeapon(i);
		os << "\n�" << (i + 1) << std::endl << w;
	}
	if (getcoveringship())
		os << "Covering Ship" << *(getcoveringship()) << std::endl;
	else
		os << "Covering Ship is absent\n";
	os << "Planes info:";
	for (int i = 0; i < curpl; i++){
		os << "\nPlane �" << (i + 1) << std::endl << getPlane(i);
	}
	return os;
}

std::ostream & operator <<(std::ostream &os, const Aircover &s){
	return s.print(os);
}

Aircover::~Aircover(){}