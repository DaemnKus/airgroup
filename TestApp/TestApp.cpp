// TestApp.cpp: ���������� ����� ����� ��� ����������� ����������.
//

#include "stdafx.h"
//#include <stdio.h>
#include "iostream"
#include "..\AirGroup\Ship.cpp"
//#include "..\Airgroup\Table.h"
#include "..\AirGroup\Table.cpp"
using namespace std;
template <class Mytype>
struct  Cont{
	const static int maxsize = 25;
	int size = 0;
	Mytype comps[maxsize];	
};
template <class Mytype>
std::ostream & operator <<(std::ostream &os, const Cont <Mytype> &c){
	for (int i = 0; i < c.size; i++){
		os << (i + 1) << ". " << c.comps[i] << endl;
	}
	return os;
}
//�����
std::ostream & operator <<(std::ostream &os, const Cont <Ship *> &c){
	for (int i = 0; i < c.size; i++){
		os << (i + 1) << ". " << *(c.comps[i]) << endl;
	}
	return os;
}

template <class Mytype>
Mytype delitm(Cont <Mytype> &obj){
	int k, n;
	string msg = "";
	do{
		cout << msg << "\nMake your choice -> ";
		msg = "\nYou're wrong. Repeat, please!";
		n = getNum(k);
		cin.clear();
		_flushall();
		if (n < 0)
			throw std::exception("End of file");
		if (!n)
			break;
	} while (k<0 || k>obj.size);
	Mytype ob = obj.comps[--k];
	for (int i = k; i < --obj.size; i++){
		obj.comps[i] = obj.comps[i + 1];
	}
	return ob;
}

template class Fleet<int, string>;
template struct Pair<string, Ship>;
template struct Pair<int, Table<string, Ship>>;

extern template Table<string, Ship> * Table<int, Table<string, Ship>>::getShip(const int &)const;

int D_Add(Fleet<int, string> &), D_Del(Fleet<int, string> &), D_CntType(Fleet<int, string> &), D_TranPlane(Fleet<int, string> &),
D_Ftrans(Fleet<int, string> &), D_Time(Fleet<int, string> &), D_Rad(Fleet<int, string> &), //D_Mod(Fleet &),
D_Show(Fleet<int, string> &), D_Insert(Fleet<int, string> &), D_ShowFL(Fleet<int, string> &), D_DelFL(Fleet<int, string> &);
int dialog(const char*[], int);
int getNum(int&);
int getNum(float&);
int Add_WP(), Add_AC(), Add_CS(), Add_AS(), Add_Cap(), Add_Plane();
Weapon * Adding_weapons(Weapon *weapons, int &cwp, int maxwp);
int Adding_Ship(Ship* &covship);
const char *msgs[] = { "0. Quit", "1. Add new object", "2. Delete object", "3. Insert object to fleet", 
	"4. Delete object from fleet", "5. Count a quantity of ships by type", "6. Transport plane", 
	"7. Transport fuel", "8. Max time", "9. Radius", "10. Show fleet information", "11. Show available objects"};

const int NMsgs = sizeof(msgs) / sizeof(msgs[0]); // ���������� �����������

int(*fptr[])(Fleet<int, string> &) = { nullptr, D_Add, D_Del, D_Insert, D_DelFL, D_CntType, D_TranPlane, D_Ftrans, D_Time, D_Rad,
D_ShowFL, D_Show };
std::string Type[] = { "", "Weapon", "Plane", "Captain", "Aircarrier", "Covership", "Aircover" };
int tcount = 7;
Cont<Weapon> WP;
Cont<Cap> CP;
Cont<Plane> PL;
Cont<Ship *> SH;

int getNum(int &a){
	std::cin >> a;
	if (std::cin.eof())
		return 0;
	if (!std::cin.good())
		return -1;
	return 1;
}

int getNum(float &a){
	std::cin >> a;
	if (std::cin.eof())
		return 0;
	if (!std::cin.good())
		return -1;
	return 1;
}

int dialog(const char *msgs[], int N)
{
	char *errmsg = "";
	int rc;
	int i, n;
	do{
		std::cin.clear();
		puts(errmsg);
		errmsg = "You are wrong. Repeat, please!";
		for (i = 0; i < N; ++i)
			std::cout << msgs[i] << std::endl;
		std::cout << "Make your choice: --> ";
		n = getNum(rc);
		std::cin.clear();
		_flushall();
		if (n == 0) // ����� ����� - ����� ������ 
			rc = 0;
	} while (rc < 0 || rc >= N || n<0);
	return rc;
}

int _tmain(int argc, _TCHAR* argv[])
{
	try{
		Fleet<int, string> fl;
		int rc;
		while (rc = dialog(msgs, NMsgs))
		if (!fptr[rc](fl))
			break;
	}
	catch (const std::exception &er){
		std::cout << std::endl << er.what() << std::endl;
		for (int i = 0; i < SH.size; i++){
			delete SH.comps[i];
		}
		return 1;
	}
	for (int i = 0; i < SH.size; i++){
		delete SH.comps[i];
	}
	return 0;
}

int D_Add(Fleet<int, string> &fl){
	std::cout << "\nChoose the type of object you want to add:\n";
	for (int i = 1; i < tcount; i++){
		std::cout << i << ". " << Type[i] << std::endl;
	}
	int rc;
	std::string msg("");
	do{
		std::cout << msg << "\nMake your choice -> ";
		msg = "\nWrong type, repeat entering, please!";
		if (getNum(rc) < 1)
			return 0;
	} while (rc<0 || rc > (tcount-1));

	switch (rc){
	case 1:
		if (!Add_WP())
			return 0;
		break;
	case 4:
		if (!Add_AC())
			return 0;
		break;
	case 5:
		if (!Add_CS())
			return 0;
		break;
	case 6:
		if (!Add_AS())
			return 0;
		break;
	case 2:
		if (!Add_Plane())
			return 0;
		break;
	case 3:
		if (!Add_Cap())
			return 0;
		break;
	}
	return 1;
}

int Add_WP(){
	if (WP.size < WP.maxsize){
		std::cout << "Name of weapon -> ";
		std::string name;
		std::cin >> name;
		if (!std::cin.good())
			return 0;
		std::cout << "Name of ammunition -> ";
		std::string amm;
		std::cin >> amm;
		if (!std::cin.good())
			return 0;
		std::cout << "Type of weapon -> ";
		int t;
		if (getNum(t) < 1)
			return 0;
		std::cout << "Destruction -> ";
		float d;
		if (getNum(d) < 1)
			return 0;
		std::cout << "Firespeed -> ";
		int f;
		if (getNum(f) < 1)
			return 0;
		std::cout << "Quantity -> ";
		int q;
		if (getNum(q) < 1)
			return 0;
		Weapon w(name, amm, d, t, f, q);
		WP.comps[WP.size++] = w;
		std::cout << "Weapon\n" << w << "\n Success\n";
	}
	else{
		std::cout << "Cannot add more than " << WP.maxsize << " weapons\n";
	}
	return 1;
}

Weapon * Adding_weapons(Weapon *weapons, int &cwp, int maxwp){
	std::cout << "Adding weapons:\n";
	int rc;
	std::string msg = "";
	const char *aweap[] = { "0. Exit from adding", "1. Use existing", "2. Add new" };
	while (rc = dialog(aweap, 3)){
		/*cout << "0. Exit from adding\n1. Use existing\n2. Add new\n";
		string msg = "";
		do{
		cout << msg << "\nMake your choice ->  ";
		msg = "\nYou're wrong. Repeat, please!";
		if (getNum(rc) < 1)
		return nullptr;
		} while (rc<0 || rc>2);*/
		if (cwp < maxwp){
			switch (rc){
			case 1:
				std::cout << "Weapons list:\n";
				for (int i = 0; i < WP.size; i++){
					std::cout << (i + 1) <<". "<< WP.comps[i].getName() << std::endl;
				}
				int k;
				do{
					std::cout << msg << "\nChoose weapon -> ";
					int n = getNum(k);
					if (n < 0)
						return nullptr;
					if (!n)
						break;
					std::cin.clear();
					_flushall();
				} while (k<1 || k>WP.size);
				weapons[cwp++] = WP.comps[k - 1];
				cout << "\nSuccess\n";
				break;
			case 2:
				if (!Add_WP())
					return 0;
				weapons[cwp++] = WP.comps[WP.size - 1];
				cout << "\nSuccess\n";
				break;
			default:
				break;
			}
		}
		else{
			std::cout << "Your arsenal is full\n";
			std::cin;
			std::cin.clear();
			_flushall();
		}
	}
	return weapons;
}

int Add_Plane(){
	if (PL.size < PL.maxsize){
		std::cout << "\nAdding plane:\nSpeed -> ";
		float s;
		if (getNum(s) < 1)
			return 0;
		std::cout << "\nConsumption -> ";
		float c;
		if (getNum(c) < 1)
			return 0;
		std::cout << "\nFuel -> ";
		float f;
		if (getNum(f) < 1)
			return 0;
		Weapon weapons[5];
		int cwp = 0;

		if (!Adding_weapons(weapons, cwp, 5))
			return 0;
		Plane pl(s, c, f);
		for (int i = 0; i < 5; i++){
			try{
				pl.setWeapon(weapons[i], i);
			}
			catch (const std::exception &exp){
				std::cout << "Weapon " << i << ": " << exp.what() << std::endl;
			}
		}
		PL.comps[PL.size++] = pl;
	}
	else{
		std::cout << "Cannot add more than " << PL.maxsize << " planes\n";
	}
	return 1;
}

Plane * Adding_planes(Plane planes[], int &cpl, int maxpl){
	std::cout << "\nAdding planes:\n";
	int rc;
	std::string msg = "";
	const char *aplan[] = { "0. Exit from adding", "1. Use existing", "2. Add new" };
	while (rc = dialog(aplan, 3)){
		if (cpl < maxpl){
			int f = 0;
			switch (rc){
			case 1:
				std::cout << "Planes list:\n";
				for (int i = 0; i < PL.size; i++){
					std::cout << (i + 1) << ". Plane " << (i + 1) << std::endl;
				}
				int k;
				do{
					std::cout << msg << "\nChoose plane -> ";
					msg = "\nYou're wrong. Repeat, please!";
					int n = getNum(k);
					if (n < 0)
						return nullptr;
					if (!n)
						break;
					std::cin.clear();
					_flushall();
				} while (k<1 || k>PL.size);
				std::cout << PL.comps[k - 1];
				msg = "";
				do{
					std::cout << msg << "\nDo you want to add this plane\? -> ";
					msg = "\nYou're wrong. Repeat, please!";
					int n = getNum(f);
					if (n < 0)
						return nullptr;
					if (!n)
						break;
					std::cin.clear();
					_flushall();
				} while (f<0 || f>1);
				if (f)
					planes[cpl++] = PL.comps[k - 1];
				cout << "\nSuccess\n";
				break;
			case 2:
				if (!Add_Plane())
					return nullptr;
				planes[cpl++] = PL.comps[PL.size - 1];
				cout << "\nSuccess\n";
				break;
			default:
				break;
			}
		}
		else{
			std::cout << "Your air fleet is full\n";
			std::cin;
			std::cin.clear();
			_flushall();
		}
	}
	return planes;
}

int Add_Cap(){
	if (CP.size < CP.maxsize){
		std::cout << "Rank -> ";
		std::string r;
		std::cin >> r;
		if (!std::cin.good())
			return 0;
		std::cout << "Name -> ";
		std::string name;
		std::cin >> name;
		if (!std::cin.good())
			return 0;
		std::cout << "Surname -> ";
		std::string srnm;
		std::cin >> srnm;
		if (!std::cin.good())
			return 0;
		std::cout << "Midname -> ";
		std::string midnm;
		std::cin >> midnm;
		if (!std::cin.good())
			return 0;
		std::cout << "Experience -> ";
		int exp;
		if (getNum(exp) < 1)
			return 0;
		Cap ncap = { r, name, srnm, midnm, exp };
		CP.comps[CP.size++] = ncap;
	}
	else{
		std::cout << "Cannot add more than " << CP.maxsize << " captains\n";
	}
	return 1;
}

int Add_AC(){
	/*cout << "Enter the number of esquadr -> ";
	int n;
	if (getNum(n) < 1)
		return 0;
	if (n>fl.getesqcount()){
		cout << "Your esquadr doesn't exist\n";
		return 1;
	}*/
	if (SH.size < SH.maxsize){
		std::cout << "Name of aircarrier -> ";
		std::string name;
		std::cin >> name;
		if (!std::cin.good())
			return 0;
		Cap cp;
		if (!CP.size){
			if (!Add_Cap())
				return 0;
			cp = CP.comps[--CP.size];
		}
		else{
			const char *altCap[] = { "1. Use existing free captain", "2. Add new" };
			std::string msg = "";
			int n;
			if (n = dialog(msgs, 2))
				switch (n){
				case 1:
					std::cout << "\nFree captains:\n";
					for (int i = 0; i < CP.size; i++){
						Cap cp1 = CP.comps[i];
						std::cout << (i + 1) << ". " << cp1.srnm << " " << cp1.name << " " << cp1.midnm << std::endl;
					}
					int k;
					do{
						std::cout << msg << "\nMake your choice ->";
						msg = "\nYou're wrong. Repeat, please!";
						if (getNum(k) < 1)
							return 0;
						std::cin.clear();
						_flushall();
					} while (k<1 || k>CP.size);
					cp = CP.comps[--k];
					for (int i = k; i < CP.size; i++){
						CP.comps[i] = CP.comps[i + 1];
					}
					CP.size--;
					break;
				case 2:
					if (!Add_Cap())
						return 0;
					cp = CP.comps[--CP.size];
					break;
			}
		}
		std::cout << "\nSpeed -> ";
		float s;
		if (getNum(s) < 1)
			return 0;
		std::cout << "\nConsumption -> ";
		float c;
		if (getNum(c) < 1)
			return 0;
		//return 1;
		std::cout << "\nFuel -> ";
		float f;
		if (getNum(f) < 1)
			return 0;
		std::cout << "\nNumber of members -> ";
		int m;
		if (getNum(m) < 1)
			return 0;
		Weapon weapons[25];
		int cwp = 0;

		if (!Adding_weapons(weapons, cwp, 25))
			return 0;

		Plane planes[10];
		int cpl = 0;
		if (!Adding_planes(planes, cpl, 10))
			return 0;

		Aircarrier ac(name, cp, s, c, f, m);
		for (int i = 0; i < cwp; i++){
			ac.setWeapon(weapons[i], i);
		}
		for (int i = 0; i < cpl; i++){
			ac.setPlane(planes[i], i);
		}

		Ship *sh = new Aircarrier(ac);
		SH.comps[SH.size++] = sh;
	}
	else{
		std::cout << "\nCannot add more than " << SH.maxsize << " ships\n";
	}
	return 1;
}

int Add_CS(){
	if (SH.size < SH.maxsize){
		std::cout << "Name of covership -> ";
		std::string name;
		std::cin >> name;
		if (!std::cin.good())
			return 0;
		Cap cp;
		if (!CP.size){
			if (!Add_Cap())
				return 0;
			cp = CP.comps[--CP.size];
		}
		else{
			const char *altCap[] = { "1. Use existing free captain", "2. Add new" };
			std::string msg = "";
			int n;
			if (n = dialog(msgs, 2))
				switch (n){
				case 1:
					std::cout << "\nFree captains:\n";
					for (int i = 0; i < CP.size; i++){
						Cap cp1 = CP.comps[i];
						std::cout << (i + 1) << ". " << cp1.srnm << " " << cp1.name << " " << cp1.midnm << std::endl;
					}
					int k;
					do{
						std::cout << msg << "\nMake your choice ->";
						msg = "\nYou're wrong. Repeat, please!";
						if (getNum(k) < 1)
							return 0;
						std::cin.clear();
						_flushall();
					} while (k<1 || k>CP.size);
					cp = CP.comps[--k];
					for (int i = k; i < CP.size; i++){
						CP.comps[i] = CP.comps[i + 1];
					}
					CP.size--;
					break;
				case 2:
					if (!Add_Cap())
						return 0;
					cp = CP.comps[--CP.size];
					break;
			}
		}
		std::cout << "\nSpeed -> ";
		float s;
		if (getNum(s) < 1)
			return 0;
		std::cout << "\nConsumption -> ";
		float c;
		if (getNum(c) < 1)
			return 0;
		//return 1;
		std::cout << "\nFuel -> ";
		float f;
		if (getNum(f) < 1)
			return 0;
		std::cout << "\nNumber of members -> ";
		int m;
		if (getNum(m) < 1)
			return 0;
		Weapon weapons[25];
		int cwp = 0;

		if (!Adding_weapons(weapons, cwp, 25))
			return 0;

		std::cout << "\nSetting covered ship:\n";
		const char *altcov[] = { "0. Nothing", "1. Select ship", "2. Add new ship" };
		int rc;
		Ship *covship = nullptr;
		std::string msg = "";
		if (rc = dialog(altcov, 3)){
			switch (rc){
				int k;
			case 1:
				for (int i = 0; i < SH.size; i++){
					std::cout << (i + 1) << ". " << SH.comps[i]->getName() << " - " << Type[SH.comps[i]->iAm() + 3] << std::endl;
				}
				do{
					std::cout << msg << "\nMake your choice -> ";
					msg = "You're wrong. Repeat, please!";
					if (getNum(k) < 1)
						return 0;
					std::cin.clear();
					_flushall();
				} while (k<1 || k >SH.size);
				covship = SH.comps[k - 1];
				break;
			case 2:
				for (int i = 4; i < tcount; i++){
					std::cout << (i - 3) << ". " << Type[i] << std::endl;
				}
				do{
					std::cout << msg << "\nMake your choice -> ";
					msg = "You're wrong. Repeat, please!";
					if (getNum(k) < 1)
						return 0;
					std::cin.clear();
					_flushall();
				} while (k<1 || k >3);
				int(*fsh[])() = { nullptr, Add_AC, Add_CS, Add_AS };
				if (!(fsh[k])()){
					return 0;
				}
				covship = SH.comps[SH.size-1];
				break;
			}
		}
		Covership cs(name, cp, s, c, f, m, covship);
		for (int i = 0; i < cwp; i++){
			cs.setWeapon(weapons[i], i);
		}
		Ship *sh = new Covership(cs);
		SH.comps[SH.size++] = sh;
	}
	else{
		std::cout << "\nCannot add more than " << SH.maxsize << " ships\n";
	}
	return 1;
}

int Add_AS(){
	if (SH.size < SH.maxsize){
		std::cout << "Name of Aircover -> ";
		std::string name;
		std::cin >> name;
		if (!std::cin.good())
			return 0;
		Cap cp;
		if (!CP.size){
			if (!Add_Cap())
				return 0;
			cp = CP.comps[--CP.size];
		}
		else{
			const char *altCap[] = { "1. Use existing free captain", "2. Add new" };
			std::string msg = "";
			int n;
			if (n = dialog(msgs, 2))
				switch (n){
				case 1:
					std::cout << "\nFree captains:\n";
					for (int i = 0; i < CP.size; i++){
						Cap cp1 = CP.comps[i];
						std::cout << (i + 1) << ". " << cp1.srnm << " " << cp1.name << " " << cp1.midnm << std::endl;
					}
					int k;
					do{
						std::cout << msg << "\nMake your choice ->";
						msg = "\nYou're wrong. Repeat, please!";
						if (getNum(k) < 1)
							return 0;
						std::cin.clear();
						_flushall();
					} while (k<1 || k>CP.size);
					cp = CP.comps[--k];
					for (int i = k; i < CP.size; i++){
						CP.comps[i] = CP.comps[i + 1];
					}
					CP.size--;
					break;
				case 2:
					if (!Add_Cap())
						return 0;
					cp = CP.comps[--CP.size];
					break;
			}
		}
		std::cout << "\nSpeed -> ";
		float s;
		if (getNum(s) < 1)
			return 0;
		std::cout << "\nConsumption -> ";
		float c;
		if (getNum(c) < 1)
			return 0;
		std::cout << "\nFuel -> ";
		float f;
		if (getNum(f) < 1)
			return 0;
		std::cout << "\nNumber of members -> ";
		int m;
		if (getNum(m) < 1)
			return 0;
		Weapon weapons[25];
		int cwp = 0;

		if (!Adding_weapons(weapons, cwp, 25))
			return 0;

		Plane planes[10];
		int cpl = 0;
		if (!Adding_planes(planes, cpl, 10))
			return 0;
		
		std::cout << "\nSetting covered ship:\n";
		const char *altcov[] = { "0. Nothing", "1. Select ship", "2. Add new ship" };
		int rc;
		Ship *covship = nullptr;
		std::string msg = "";
		if (rc = dialog(altcov, 3)){
			switch (rc){
				int k;

			case 1:
				for (int i = 0; i < SH.size; i++){
					std::cout << (i + 1) << ". " << SH.comps[i]->getName() << " - " << Type[SH.comps[i]->iAm() + 3] << std::endl;
				}
				do{
					std::cout << msg << "\nMake your choice -> ";
					msg = "You're wrong. Repeat, please!";
					if (getNum(k) < 1)
						return 0;
					std::cin.clear();
					_flushall();
				} while (k<1 || k >SH.size);
				covship = SH.comps[k - 1];
				break;
			case 2:
				for (int i = 4; i < tcount; i++){
					std::cout << (i - 3) << ". " << Type[i] << std::endl;
				}
				do{
					std::cout << msg << "\nMake your choice -> ";
					msg = "You're wrong. Repeat, please!";
					if (getNum(k) < 1)
						return 0;
					std::cin.clear();
					_flushall();
				} while (k<1 || k >3);
				int(*fsh[])() = { nullptr, Add_AC, Add_CS, Add_AS };
				if (!(fsh[k])()){
					return 0;
				}
				covship = SH.comps[SH.size-1];
				break;
			}
		}
		Aircover ac(name, cp, s, c, f, m, covship);
		for (int i = 0; i < cwp; i++){
			ac.setWeapon(weapons[i], i);
		}
		for (int i = 0; i < cpl; i++){
			ac.setPlane(planes[i], i);
		}
			Ship *sh = new Aircover(ac);
			SH.comps[SH.size++] = sh;
	}
	else{
		std::cout << "\nCannot add more than " << SH.maxsize << " ships\n";
	}
	return 1;
}

int D_Show(Fleet<int, string> &fl){
	cout << "\nWeapons:\n" << WP << "\nCaps:\n" << CP<< "\nShips: \n" << SH << "\nPlanes: \n" << PL<<endl;
	return 1;
}

int D_Del(Fleet<int, string> &fl){
	std::cout << "\nChoose the type of object you want to delete:\n";
	for (int i = 1; i < 4; i++){
		std::cout << i << ". " << Type[i] << std::endl;
	}
	cout << "4. Ship\n";
	int rc;
	std::string msg("");
	do{
		std::cout << msg << "\nMake your choice ->";
		msg = "\nWrong type, repeat entering, please!";
		if (getNum(rc) < 1)
			return 0;
	} while (rc<0 || rc >(tcount - 1));
	try{

		switch (rc){


		case 1:
			if (WP.size){
				cout << "\nWeapons list:\n";
				for (int i = 0; i < WP.size; i++){
					cout << (i + 1) << ". " << WP.comps[i].getName() << endl;
				}
				delitm <Weapon>(WP);
			}
			else{
				cout << "\nNo objects to delete\n";
			}
			break;
		case 2:
			if (PL.size){
				cout << "\nPlanes list:\n";
				for (int i = 0; i < PL.size; i++){
					cout << (i + 1) << ". Plane " << (i + 1) << endl;
				}
				delitm <Plane>(PL);
			}
			else{
				cout << "\nNo objects to delete\n";
			}
			break;
		case 3:
			if (CP.size){
				cout << "\nFree caps list:\n";
				for (int i = 0; i < CP.size; i++){
					cout << (i + 1) << ". " << CP.comps[i].srnm << " " << CP.comps[i].name << " " << CP.comps[i].midnm << endl;
				}
				delitm <Cap>(CP);
			}
			else{
				cout << "\nNo objects to delete\n";
			}
			break;
		case 4:
			if (SH.size){
				cout << "\nShips list:\n";
				for (int i = 0; i < SH.size; i++){
					cout << (i + 1) << ". " << SH.comps[i]->getName() << endl;
				}
				Ship *sh = delitm <Ship *>(SH);
				delete sh;
			}
			else{
				cout << "\nNo objects to delete\n";
			}
			break;
		}
	}
	catch (const std::exception &err){
		cout << err.what() << endl;
	}
	return 1;

}

int Adding_Ship(Ship* &covship){
	const char *altcov[] = { "1. Select ship", "2. Add new ship" };
	int rc;
	//Ship *covship = nullptr;
	std::string msg = "";
	if (rc = dialog(altcov, 2)){
		switch (rc){
			int k;

		case 1:
			for (int i = 0; i < SH.size; i++){
				std::cout << (i + 1) << ". " << SH.comps[i]->getName() << " - " << Type[SH.comps[i]->iAm() + 3] << std::endl;
			}
			do{
				std::cout << msg << "\nMake your choice -> ";
				msg = "You're wrong. Repeat, please!";
				if (getNum(k) < 1)
					return 0;
				std::cin.clear();
				_flushall();
			} while (k<1 || k >SH.size);
			covship = SH.comps[k - 1];
			break;
		case 2:
			for (int i = 4; i < tcount; i++){
				std::cout << (i - 3) << ". " << Type[i] << std::endl;
			}
			do{
				std::cout << msg << "\nMake your choice -> ";
				msg = "You're wrong. Repeat, please!";
				if (getNum(k) < 1)
					return 0;
				std::cin.clear();
				_flushall();
			} while (k<1 || k >3);
			int(*fsh[])() = { nullptr, Add_AC, Add_CS, Add_AS };
			if (!(fsh[k])()){
				return 0;
			}
			covship = SH.comps[SH.size - 1];
			break;
		}
	}
	return 1;
}

int D_Insert(Fleet<int, string> &fl){
	cout << "\nChoose the type of object you want to insert into fleet:\n1. Ship\n2. Plane\n";
	int rc;
	std::string msg("");
	do{
		std::cout << msg << "\nMake your choice ->";
		msg = "\nWrong type, repeat entering, please!";
		if (getNum(rc) < 1)
			return 0;
	} while (rc<0 || rc >2);
	try{
		int k = 0;
		string s;
		Ship *sh = nullptr;
		switch (rc){
		case 1:
			if (!Adding_Ship(sh))
				return 0;
			cout << "\nNumber of esquadr -> ";
			if (getNum(k) < 1)
				return 0;
			cout << "Call sign -> ";
			cin >> s;
			if (!cin.good()){
				return 0;
			}
			fl.insert(s, *sh, k-1);
			break;
		case 2:
			Plane pl[1];
			int curpl = 0;
			if (!Adding_planes(pl, curpl, 1)){
				return 0;
			}
			cout << "Call sign of ship -> ";
			cin >> s;
			if (!cin.good()){
				return 0;
			}
			fl.insert(pl[0], s);
			break;
		}
	}
	catch (const exception &ex){
		cout << ex.what() << endl;
	}
	return 1;
}

int D_CntType(Fleet<int, string> &fl){
	cout << "\nChoose the type of ship you want to count:\n";
	for (int i = 4; i < tcount; i++){
		cout << (i - 3) << ". " << Type[i] << endl;
	}
	string msg = "";
	int k;
	do{
		cout << msg << "Make your choice -> ";
		msg = "\nYou're wrong. Repeat, please!\n";
		if (getNum(k) < 1)
			return 0;
		cin.clear();
		_flushall();
	} while (k<1 || k>3);
	cout<< "There are "<<fl.getcount(k)<< " "<<Type[3+k]<<"in the fleet\n";
	return 1;
}

int D_TranPlane(Fleet<int, string> &fl){
	cout << "\nEnter the call sign of the source ship -> ";
	string src;
	cin >> src;
	if (!cin.good())
		return 0;
	cout << "Enter the call sign of the destination ship -> ";
	string dst;
	cin >> dst;
	if (!cin.good())
		return 0;
	cout << "Number of plane -> ";
	int n;
	if (getNum(n) < 1)
		return 0;
	try{
		fl.transport(dst, src, --n);
	}
	catch (const exception &ex){
		cout << ex.what() << endl;
	}
	return 1;
}

int D_Ftrans(Fleet<int, string> &fl){
	cout << "\nEnter the call sign of the source ship -> ";
	string src;
	cin >> src;
	if (!cin.good())
		return 0;
	cout << "Enter the call sign of the destination ship -> ";
	string dst;
	cin >> dst;
	if (!cin.good())
		return 0;
	cout << "Amount of fuel -> ";
	float n;
	if (getNum(n) < 1)
		return 0;
	try{
		fl.fueltrans(dst, src, n);
	}
	catch (const exception &ex){
		cout << ex.what() << endl;
	}
	return 1;
}

int D_Time(Fleet<int, string> &fl){
	try{
		cout << "Maxtime: " << fl.maxtime() << endl;
	}
	catch (const std::exception &ex){
		cout << ex.what() << endl;
	}
	return 1;
}

int D_Rad(Fleet<int, string> &fl){
	cout << "\nEnter the number of esquadr -> ";
	int n;
	if (getNum(n) < 1)
		return 0;
	try{
		cout << "Radius: " << fl.radius(--n) << endl;
	}
	catch (const std::exception &ex){
		cout << ex.what() << endl;
	}

	return 1;
}

int D_ShowFL(Fleet<int, string> &fl){
	cout << "Fleet: ";
	cout << "Commander:" << fl.getcap();
	for (int i=0; i < fl.getesqcount(); i++){
		cout << "Esquadr " << (i + 1) << endl << fl.nesq(i);
	}
	return 1;
}

int D_DelFL(Fleet<int, string> &fl){
	cout << "\nChoose the object you want to delete:\n1. Ship\n2. Plane\n";
	int k;
	string msg = "";
	do{
		cout << msg << "\nMake your choice ->";
		msg = "\nYou're wrong. Repeat, please!";
		if (getNum(k) < 1)
			return 0;
		cin.clear();
		_flushall();
	} while (k<1 || k>2);
	cout << "Enter call sign -> ";
	string s;
	cin >> s;
	if (!cin.good())
		return 0;
	try{
		switch (k){
			int n;
		case 1:
			fl.delShip(s);
			break;
		case 2:
			cout << "Enter number of plane -> ";
			if (getNum(n) < 1)
				return 0;
			fl.delPlane(s, --n);
			break;
		}
	}
	catch (const exception &ex){
		cout << ex.what() << endl;
	}
	return 1;
}
/*
int D_ModFL(Fleet<int, string> &fl){
	if (!fl.getesqcount())
		cout << "\nFleet has no any objects to modificate\n";
	else{
		std::cout << "\nChoose the type of object you want to modificate:\n1. Ship\n2.Plane\n";
		
		int rc;
		std::string msg("");
		do{
			std::cout << msg << "\nMake your choice -> ";
			msg = "\nWrong type, repeat entering, please!";
			if (getNum(rc) < 1)
				return 0;
			cin.clear();
			_flushall();
		} while (rc<0 || rc >(tcount - 1));
		cout << "\nEnter call sign of ship -> ";
		string s;
		cin >> s;
		if (!cin.good())
			return 0;
		try{
			const char *alter[] = { "1. Name", "2. Captain", "3.Speed", "4. Consumption", "5. Fuel", "6. Number of members", "7. Weapons" };

			switch (rc){			
				Ship *sh;

				
			case 1:
				sh =&(fl[s]);
				if (!sh)
					cout << "\"" << s << "\" - ship with this call sign doesn't exist\n";
				int rl;
				if (rl = dialog(alter, 6)){

				}
				break;
			case 2:

				break;
		}
		}
		catch (const exception &ex){
			cout << ex.what() << endl;
		}
	}
	return 1;
}*/